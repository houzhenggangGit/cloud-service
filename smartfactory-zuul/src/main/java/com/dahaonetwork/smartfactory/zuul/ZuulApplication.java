package com.dahaonetwork.smartfactory.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.undertow.UndertowEmbeddedServletContainerFactory;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
/**
 * 
 * @author liguobao
 *
 */
@EnableAutoConfiguration
@SpringBootApplication
@EnableZuulProxy //声明一个zuul代理
@EnableDiscoveryClient //注册中心注册服务
public class ZuulApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ZuulApplication.class, args);
	}
	
  /*  @Bean
    public EmbeddedServletContainerFactory servletContainer() {
    	UndertowEmbeddedServletContainerFactory factory = 
                      new UndertowEmbeddedServletContainerFactory();
        return factory;
     }*/

}
