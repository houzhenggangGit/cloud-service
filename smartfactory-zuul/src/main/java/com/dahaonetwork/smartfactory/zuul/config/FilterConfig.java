package com.dahaonetwork.smartfactory.zuul.config;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dahaonetwork.smartfactory.zuul.filter.CommGZIPFilter;

@Configuration
public class FilterConfig {
	/**
	 * @Title: accessFilter
	 * @Description: 路由过滤器
	 * @return AccessFilter
	 */
	/*@Bean
	public AccessFilter accessFilter(){
		return new AccessFilter();
	}
	@Bean
	public RouteFilter routeFilter(){
		return new RouteFilter();
	}*/
	/** gzip过滤器urlpatterns */
	@Value("${platform.config.gzipfilter.url-patterns}")
	private String url_patterns;
	
	/** 过滤器排除请求 */
	@Value("${platform.config.gzipfilter.exclude}")
	private String exclude;
	
	/**
	 * @Title: gzipFilterRegistrationBean
	 * @Description: 添加gzip过滤器
	 * @return FilterRegistrationBean
	 */
	@Bean
	public FilterRegistrationBean gzipFilterRegistrationBean(){
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(gzipFilter());

		for(String url : url_patterns.split(",")){
			registration.addUrlPatterns(url);
		}
		
		registration.addInitParameter("exclude", exclude);
		registration.setName("gzipFilter");
		return registration;
	}
	
	@Bean(value = "gzipFilter")
	public Filter gzipFilter(){
		return new CommGZIPFilter();
	}

}
