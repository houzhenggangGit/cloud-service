/*
 * Powered By [aisino_plateform]
 * Since 2017 - 2018
 */
package com.dahaonetwork.smartfactory.authserver.mapper;

import java.util.Map;

import com.dahaonetwork.smartfactory.authserver.model.User;


public interface UserMapper  {
	public User getUserByloginIds(Map<?, ?> paramMap);

}