package com.dahaonetwork.smartfactory.authserver.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.dahaonetwork.smartfactory.authserver.mapper.UserMapper;
import com.dahaonetwork.smartfactory.authserver.model.MyUserDetails;
import com.dahaonetwork.smartfactory.authserver.model.User;
import com.dahaonetwork.smartfactory.constant.Constants;

@Service("userInfo")
public class UserInfoService implements UserDetailsService{
	
	@Autowired
	private UserMapper userMapper;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		Map<String, String> paramMap=new HashMap<String, String>();
		paramMap.put("loginId", username);
		User user=userMapper.getUserByloginIds(paramMap);
		
		if(user ==null) {
			throw new BadCredentialsException(Constants.getReturnStr(Constants.USER_NOT_FOUND, Constants.USER_NOT_FOUND_TIPS));
		}
		MyUserDetails userDetails = new MyUserDetails();
		userDetails.setUserName(username);
		userDetails.setPassword(user.getPassword());
		userDetails.setUser(user);
		return userDetails;
	}

}
