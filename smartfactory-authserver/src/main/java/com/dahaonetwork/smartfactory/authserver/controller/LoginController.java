package com.dahaonetwork.smartfactory.authserver.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {
	
 	@RequestMapping("/login.do")
    public Object login() {
        Map<String,String> map=new HashMap<>();
        map.put("code", "201");
        map.put("message", "请登录");
		return map;
    }
}
