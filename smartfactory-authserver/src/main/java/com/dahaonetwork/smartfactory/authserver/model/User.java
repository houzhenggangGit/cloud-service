/*
 * Powered By [aisino_plateform]
 * Since 2017 - 2018
 */
package com.dahaonetwork.smartfactory.authserver.model;

import org.hibernate.validator.constraints.Length;

import com.alibaba.fastjson.annotation.JSONField;


public class User {
	
	//可以直接使用: @Length(max=50,message="用户名长度不能大于50")显示错误消息
	//columns START
    /**
     * 员工id       db_column: id 
     */	
	@Length(max=32)
	private java.lang.String id;
    /**
     * 员工编号       db_column: number 
     */	
	@Length(max=30)
	private java.lang.String number;
    /**
     * 员工名称       db_column: name 
     */	
	@Length(max=50)
	private java.lang.String name;
    /**
     * 性别       db_column: sex 
     */	
	@Length(max=10)
	private java.lang.String sex;
    /**
     * 员工电话       db_column: phone 
     */	
	@Length(max=20)
	private java.lang.String phone;
    /**
     * 所属工厂车间编码       db_column: factory_code 
     */	
	@Length(max=30)
	private java.lang.String factoryCode;
    /**
     * 员工班次id       db_column: user_shift_id 
     */	
	@Length(max=32)
	private java.lang.String userShiftId;
    /**
     * 备注       db_column: remark 
     */	
	@Length(max=200)
	private java.lang.String remark;
    /**
     * 创建时间       db_column: create_time 
     */	
	
	@JSONField (format="yyyy-MM-dd HH:mm:ss")
	private java.util.Date createTime;
    /**
     * 是否有效       db_column: active 
     */	
	
	private java.lang.Integer active;
    /**
     * 租户id       db_column: tenant_id 
     */	
	
	private java.lang.Integer tenantId;
	/**
	 * 组别名字
	 */
	private java.lang.String factoryName;
	/**
	 * 工作开始时间
	 */
	private java.lang.String startWorkTime;
	/**
	 * 工作结束时间
	 */
	private java.lang.String endWorkTime;
	
	//新增字段，把密码验证放到用户表中
	/**
	 * 登录id
	 */
	private String loginId;
	/**
	 * 登录密码
	 */
	private String password;
	/**
	 * 登录名称
	 */
	private String loginName;
	/**
	 * 用户类别
	 */
	private String userCategory;
	
	//columns END
	
	
	public void setId(java.lang.String value) {
		this.id = value;
	}
	
	public java.lang.String getStartWorkTime() {
		return startWorkTime;
	}

	public void setStartWorkTime(java.lang.String startWorkTime) {
		this.startWorkTime = startWorkTime;
	}

	public java.lang.String getEndWorkTime() {
		return endWorkTime;
	}

	public void setEndWorkTime(java.lang.String endWorkTime) {
		this.endWorkTime = endWorkTime;
	}

	public java.lang.String getFactoryName() {
		return factoryName;
	}

	public void setFactoryName(java.lang.String factoryName) {
		this.factoryName = factoryName;
	}

	public java.lang.String getId() {
		return this.id;
	}
	public void setNumber(java.lang.String value) {
		this.number = value;
	}
	
	public java.lang.String getNumber() {
		return this.number;
	}
	public void setName(java.lang.String value) {
		this.name = value;
	}
	
	public java.lang.String getName() {
		return this.name;
	}
	public void setSex(java.lang.String value) {
		this.sex = value;
	}
	
	public java.lang.String getSex() {
		return this.sex;
	}
	public void setPhone(java.lang.String value) {
		this.phone = value;
	}
	
	public java.lang.String getPhone() {
		return this.phone;
	}
	public void setFactoryCode(java.lang.String value) {
		this.factoryCode = value;
	}
	
	public java.lang.String getFactoryCode() {
		return this.factoryCode;
	}
	public void setUserShiftId(java.lang.String value) {
		this.userShiftId = value;
	}
	
	public java.lang.String getUserShiftId() {
		return this.userShiftId;
	}
	public void setRemark(java.lang.String value) {
		this.remark = value;
	}
	
	public java.lang.String getRemark() {
		return this.remark;
	}
	public void setCreateTime(java.util.Date value) {
		this.createTime = value;
	}
	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	public void setActive(java.lang.Integer value) {
		this.active = value;
	}
	
	public java.lang.Integer getActive() {
		return this.active;
	}
	public void setTenantId(java.lang.Integer value) {
		this.tenantId = value;
	}
	
	public java.lang.Integer getTenantId() {
		return this.tenantId;
	}
    
	
	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserCategory() {
		return userCategory;
	}

	public void setUserCategory(String userCategory) {
		this.userCategory = userCategory;
	}

	@Override
	public int hashCode() {
		final Integer prime = 31;
		Integer result = 1;
		result = prime * result + ((active == null) ? 0 : active.hashCode());
		result = prime * result + ((createTime == null) ? 0 : createTime.hashCode());
		result = prime * result + ((factoryCode == null) ? 0 : factoryCode.hashCode());
		result = prime * result + ((factoryName == null) ? 0 : factoryName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		result = prime * result + ((sex == null) ? 0 : sex.hashCode());
		result = prime * result + ((tenantId == null) ? 0 : tenantId.hashCode());
		result = prime * result + ((userShiftId == null) ? 0 : userShiftId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (active == null) {
			if (other.active != null)
				return false;
		} else if (!active.equals(other.active))
			return false;
		if (createTime == null) {
			if (other.createTime != null)
				return false;
		} else if (!createTime.equals(other.createTime))
			return false;
		if (factoryCode == null) {
			if (other.factoryCode != null)
				return false;
		} else if (!factoryCode.equals(other.factoryCode))
			return false;
		if (factoryName == null) {
			if (other.factoryName != null)
				return false;
		} else if (!factoryName.equals(other.factoryName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		if (sex == null) {
			if (other.sex != null)
				return false;
		} else if (!sex.equals(other.sex))
			return false;
		if (tenantId == null) {
			if (other.tenantId != null)
				return false;
		} else if (!tenantId.equals(other.tenantId))
			return false;
		if (userShiftId == null) {
			if (other.userShiftId != null)
				return false;
		} else if (!userShiftId.equals(other.userShiftId))
			return false;
		return true;
	}
	
}

