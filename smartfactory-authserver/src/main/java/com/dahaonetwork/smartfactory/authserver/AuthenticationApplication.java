package com.dahaonetwork.smartfactory.authserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.transaction.annotation.EnableTransactionManagement;
/**
 * 基于oauth2 的认证服务器
 * @author liguobao
 *	Token Name: access_token
 *  Auth URL: http://localhost:8043/uaa/oauth/authorize
 *	Access Token URL: http://localhost:8043/uaa/oauth/token
 */
//@EnableDiscoveryClient
@SpringBootApplication
@Configuration
@EnableCaching
@EnableAuthorizationServer
@EnableTransactionManagement
@MapperScan(basePackages = {
		"com.dahaonetwork.smartfactory.authserver.mapper"
		})
public class AuthenticationApplication {
	/** 主类  */
	public static void main(String[] args) {
        SpringApplication.run(AuthenticationApplication.class, args);
    }

}
