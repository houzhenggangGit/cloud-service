package com.dh.chain;

/**
 * 链式调用
 * 实现链式调用方法是因为调用的方法返回值是对象本身（return this;）
 * @author liguobao
 *
 */
public class Student {
    /**
     * 不能通过new初始化
     */
    private Student(){}

    public static Builder builder(){
        return new Builder();
    } 

    static class Builder{
        /**
         * 姓名
         */
        private String name;
        /**
         * 年龄
         */
        private int age;
        /**
         * 学号
         */
        private String no;
        /**
         * 年级
         */
        private String grade;
        /**
         * 专业
         */
        private String major;

        public Builder stuName(String name){
            this.name = name;
            return this;
        }

        public Builder stuAge(int age){
            this.age = age;
            return this;
        }

        public Builder stuNo(String no){
            this.no = no;
            return this;
        }

        public Builder stuGrade(String grade){
            this.grade = grade;
            return this;
        }

        public Builder stuMajor(String major){
            this.major = major;
            return this;
        }
    }
    
    public static void main(String[] args) {
        Student.builder()
        .stuName("chenxuxu")
        .stuAge(22)
        .stuGrade("13级")
        .stuMajor("软件工程")
        .stuNo("123456789");
    }


}
