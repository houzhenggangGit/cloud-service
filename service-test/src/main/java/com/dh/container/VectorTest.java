package com.dh.container;

import java.util.Vector;

public class VectorTest {
	
	public static void main(String args[]) {
		Vector v=new Vector<>();
		for(int i=0;i<100000;i++) {
			v.add(i);
		}
		
		for(int i=0;i<100;i++) {
			new Thread(new Runnable() {	
				@Override
				public void run() {
					// TODO Auto-generated method stub
					deleteLast(v);
				}
			}).start();
		}
		
		
		
	}
	
	public static void deleteLast(Vector v){
//		 synchronized (v) {
//			 int lastIndex  = v.size()-1;
//			 v.remove(lastIndex);
//		 }
		synchronized (v) {
			for (int i = 0; i < v.size(); i++) {
			    v.remove(i);
			}
		}
	  
	}

}
