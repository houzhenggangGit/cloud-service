package com.dh.deadlock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SellTicket implements Runnable{
	
	private int tickets = 100;
	private Lock lock = new ReentrantLock();

	@Override
	public void run() {
		while(true){
			lock.lock();
			try{
				if(tickets>0){
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName()
	                        + "正在出售第" + (tickets--) + "张票");
					
				}
				
			}finally {
				lock.unlock();
			}
		}

	}
	public static void main(String args[]){
		SellTicket str = new SellTicket();
        
        Thread tr1 = new Thread(str, "窗口1");
        Thread tr2 = new Thread(str, "窗口2");
        Thread tr3 = new Thread(str, "窗口3");
        
        //
        tr1.start();
        tr2.start();
        tr3.start();
		
	}

}
