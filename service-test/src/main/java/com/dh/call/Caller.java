package com.dh.call;

public class Caller {
    private MyCallInterface callInterface;

    public Caller(){

    }
    public void setCallInterface(MyCallInterface callInterface){
        this.callInterface=callInterface;
        call();
    }

    private  void call(){
        callInterface.printName();
    }

}
