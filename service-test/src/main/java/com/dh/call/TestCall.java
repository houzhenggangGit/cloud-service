package com.dh.call;

public class TestCall {
    public static void main(String[] args) {
        Caller caller = new Caller();
        //caller.setCallInterface(new Client());
        caller.setCallInterface(new MyCallInterface() {
            @Override
            public void printName() {
                System.out.println("123");
            }
        });
        caller.setCallInterface(()->{
            System.out.println("123123");
        });
        caller.setCallInterface(System.out::println);
    }
}
