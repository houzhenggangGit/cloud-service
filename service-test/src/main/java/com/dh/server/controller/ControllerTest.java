package com.dh.server.controller;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.dahaonetwork.smartfactory.util.ThreadLocalUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.Map;


@RestController
@RequestMapping("/users")
public class ControllerTest {

    @RequestMapping(value="v1.do")
    public String status(HttpServletRequest request, Map<String, Object> model) {
    	HttpSession session = (HttpSession) ThreadLocalUtils
				.getObjectFromThreadLocal("session");
		String userModel =(String) session
				.getAttribute("userModel");
		System.out.println(userModel);
    	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
		String str=details.getTokenValue();
        return "123";
    }
}
