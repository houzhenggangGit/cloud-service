package com.dh.Linked;

import org.junit.Test;


public class LinkedTest {
	private int size;
	private Node head;
	
	public LinkedTest(){
		this.size=0;
		head=null;
	}
	
	/**
	 * 链表中的节点类
	 * @author liguobao
	 *
	 */
	private class Node{
		private Object data;//每个节点的数据
		private Node next;//每个节点指向下一个节点的连接
		
		public Node(Object data) {
			this.data=data;
		}
		
	}
	
	/**
	 * 删除头节点
	 * @return
	 */
	public Object deleteHead(){
		Object object=head.data;
		head=head.next;
		size--;
		return object;
	}
	
	/**
	 * 在链表头添加元素
	 * @param object
	 */
	public Object addHead(Object object){
		Node newHead=new Node(object);
		if(size==0){
			head=newHead;
		}else{
			
			newHead.next=head;
			head=newHead;
		}
		size++;
		return object;
	}
	
	/**
	 * 查找指定元素
	 * @param obj
	 * @return
	 */
	public Node find(Object obj) {
		Node current=head;
		int tempsize=size;
		while(tempsize>0){
			//如果当前节点等于obj,则找到了
			if(obj.equals(current.data)){
				return current;
			}else{
				current=current.next;
			}
			tempsize--;
		}
		return null;
		
	}
	
	//删除指定的元素，删除成功返回true
	public boolean delete(Object value) {
		if(size==0){
			return false;
		}
		Node current=head;
		Node previous=head;
		
		//当前节点的值不等于value
		while(current.data!=value){
			if(current.next==null){
				return false;
			}else{
				previous=current;
				current=current.next;
			}
		}
		if(current==head){
			head=current.next;
			size--;
		}else{
			previous.next=current.next;
			size--;
		}
		return true;
	}
	
	//判断链表是否为空
    public boolean isEmpty(){
        return (size == 0);
    }
    
  //显示节点信息
    public void display(){
        if(size >0){
            Node node = head;
            int tempSize = size;
            if(tempSize == 1){//当前链表只有一个节点
                System.out.println("["+node.data+"]");
                return;
            }
            while(tempSize>0){
                if(node.equals(head)){
                    System.out.print("["+node.data+"->");
                }else if(node.next == null){
                    System.out.print(node.data+"]");
                }else{
                    System.out.print(node.data+"->");
                }
                node = node.next;
                tempSize--;
            }
            System.out.println();
        }else{//如果链表一个节点都没有，直接打印[]
            System.out.println("[]");
        }
        
    }

	/**
	 * 单链表反转
	 * @param head
	 * @return
	 */
	public Node reverseList(Node head) {
		if(null==head||null==head.next){
			return head;
		}
        Node p=head;
		Node q=head.next;
		Node r;
		head.next=null;//旧的头指针是新的尾指针，next指向null
		while (q!=null){
			r=q.next;//先保留下一个循环要处理的数据
			q.next=p;//反转
			p=q;
			q=r;
		}
		head=p;//最后q必为null,返回p作为新的头
		return head;
	}

	/**
	 *
	 * 解决思路1：快慢指针法
	 * 这是最常见的方法。思路就是有两个指针P1和P2，同时从头结点开始往下遍历链表中的所有节点。
	 *
	 * P1是慢指针，一次遍历一个节点。
	 * P2是快指针，一次遍历两个节点。
	 *
	 * 如果链表中没有环，P2和P1会先后遍历完所有的节点。
	 *
	 * 如果链表中有环，P2和P1则会先后进入环中，一直循环，并一定会在在某一次遍历中相遇。
	 *
	 * 因此，只要发现P2和P1相遇了，就可以判定链表中存在环。
	 *
	 * @param headNode
	 * @return
	 */
	public static boolean hasLoopV1(Node headNode) {

		if(headNode == null) {
			return false;
		}

		Node p = headNode;
		Node q = headNode.next;

		// 快指针未能遍历完所有节点
		while (q != null && q.next != null) {
			p = p.next; // 遍历一个节点
			q = q.next.next; // 遍历两个个节点

			// 已到链表末尾
			if (q == null) {
				return false;
			} else if (p == q) {
				// 快慢指针相遇，存在环
				return true;
			}
		}
		return false;
	}



	@Test
    public void testSingleLinkedList(){
    	LinkedTest singleList = new LinkedTest();
        singleList.addHead("A");
        singleList.addHead("B");
        singleList.addHead("C");
        singleList.addHead("D");
        //链表反转
		//Node node=singleList.reverseList(singleList.head);
        //打印当前链表信息
        singleList.display();
       //删除C
        singleList.delete("C");
        singleList.display();
        //查找B
        System.out.println(singleList.find("B"));
    }

}
