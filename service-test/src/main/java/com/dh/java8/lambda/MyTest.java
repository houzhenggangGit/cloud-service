package com.dh.java8.lambda;

import java.util.Arrays;

public class MyTest {

    public static void main(String[] args) {
        Arrays.asList("a","b","c").forEach(e-> System.out.println(e));
        Arrays.asList( "a", "b", "d" ).forEach( ( String e ) -> System.out.println( e ) );

        Arrays.asList( "a", "d", "b" ).sort( ( e1, e2 ) -> {
            int result = e1.compareTo( e2 );
            return result;
        } );

        Arrays.asList("a","b","c").forEach(System.out::println);
    }
}
