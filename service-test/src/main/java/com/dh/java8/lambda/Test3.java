package com.dh.java8.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class Test3 {
    public static void main(String[] args) {

        MyInteface i1=()->{};
        System.out.println(i1.getClass().getInterfaces()[0]);

        MyInteface2 i2=()->{};
        System.out.println(i2.getClass().getInterfaces()[0]);

        Runnable r=()->{System.out.println("hello world");};
        new Thread(r).start();

        List<String> list=Arrays.asList("a","b","c");

        List<String> list2=new ArrayList<>();
        /*list.forEach((s)->{
            list2.add(s);
        });*/
        list.forEach(list2::add);
        //Stream
        list.stream().map(item -> item.toUpperCase()).forEach(item -> System.out.println(item));
        //方法引用
        list.stream().map(String::toUpperCase).forEach(System.out::println);

        list.stream().map( item -> {
            return item.toUpperCase();
        });
        list.stream().map(Integer::parseInt);

        //输入参数是调用方法的对象
        Function<String,String> function=String::toUpperCase;
        System.out.println(function.getClass().getInterfaces()[0]);

    }
}

@FunctionalInterface
interface MyInteface{
    public void myMethod();
}

@FunctionalInterface
interface MyInteface2{
    public void myMethod2();
}