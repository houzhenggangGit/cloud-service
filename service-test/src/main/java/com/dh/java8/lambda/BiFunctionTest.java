package com.dh.java8.lambda;

import java.util.function.Function;

import java.util.function.BiFunction;

public class BiFunctionTest {

    public static void main(String[] args) {

        BiFunctionTest test=new BiFunctionTest();

        System.out.println(test.compute(2,3,(a1,a2)->a1+a2));

        System.out.println(test.compute1(2,3,(a,b)->a+b,a -> a*a));
    }

    public int compute(int a, int b, BiFunction<Integer,Integer,Integer> biFunction){
       return biFunction.apply(a,b);
    }

    /**
     * BiFunction为什么缺少compose方法：参数类型为Bifunction,得到一个结果值，应用到当前的BiFunction必须有俩个输入
     * @param a
     * @param b
     * @param bifunction
     * @param function
     * @return
     */
    public int compute1(int a, int b, BiFunction<Integer,Integer,Integer> bifunction,
                        Function<Integer,Integer> function){
        return bifunction.andThen(function).apply(a,b);

    }
}
