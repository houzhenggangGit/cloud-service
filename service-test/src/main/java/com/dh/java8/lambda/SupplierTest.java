package com.dh.java8.lambda;


import java.util.function.Supplier;

/**
 * supplier 不接受参数返回结果
 */
public class SupplierTest {

    public static void main(String[] args) {
        SupplierTest test=new SupplierTest();
        System.out.println(test.get(()-> 1));
        Supplier<Student> supplier= ()->new Student();
        System.out.println("---------------------------------");
        Supplier<Student> supplier2= Student::new;
        System.out.println(supplier2.get().getAge());

    }

    public Integer get(Supplier<Integer> supplier){
        return supplier.get();
    }
}

class Student{
    private String name="zhangsan";
    private Integer age=20;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
