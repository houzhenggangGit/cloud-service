package com.dh.java8.lambda;

import java.util.function.Function;

/**
 * 传递行为
 */
public class FunctionTest {

    public static void main(String[] args) {
        FunctionTest test=new FunctionTest();
       /* System.out.println(test.compute(1,value->{return 2*value;}));
        System.out.println(test.compute(1,value-> 2+value));*/
        System.out.println(test.compute1(2,value->value*3,value->value*value));
        System.out.println(test.compute2(2,value->value*3,value->value*value));
    }

    /**
     * 高阶函数，接收一个函数或者返回一个函数
     * @param a
     * @param function
     * @return
     */
    public int compute(int a, Function<Integer,Integer> function){
        int result=function.apply(a);
        return result;
    }

    /**
     * 之前需要得到某个值的2倍，则需要单独写方法，那么乘以3，乘以4或者是其他的都得单独封装方法：
     * 如果使用函数式编程，则只需要定义某个行为，在实际使用的时候才会具指定操作。
     * @param a
     * @return
     */
    public int add(int a){
        return a*2;
    }

    /**
     * 多个function的串联
     * @param a
     * @param function1
     * @param function2
     * @return
     */
    public int compute1(int a,Function<Integer,Integer> function1,Function<Integer,Integer> function2){
        return function1.compose(function2).apply(a);
    }

    public int compute2(int a,Function<Integer,Integer> function1,Function<Integer,Integer> function2){
        return function1.andThen(function2).apply(a);
    }
}
