package com.dh.java8.lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * lamdba 表达式传递行为
 */
public class StringCompare {

    public static void main(String[] args) {
        List<String> names= Arrays.asList("zhangsan","lisi","wangwu","zhaoliu");

       /* Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        });
        names.forEach(System.out::println);*/

        Collections.sort(names, Comparator.naturalOrder());

        Collections.sort(names,(o1, o2) -> {
            return o1.compareTo(o2);
        });

        Collections.sort(names,String::compareTo);
        System.out.println(names);

    }
}
