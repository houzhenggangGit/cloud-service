package com.dh.java8.methodReference;

public class Student {

    private String name;

    private int score;

    public Student(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    /**
     * @param student1
     * @param student2
     * @return
     */
    public static int compareStudentByScore(Student student1,Student student2){
        return  student1.getScore()-student2.getScore();
    }

    /**
     * 正确的设计
     * @param s
     * @return
     */
    public int compare(Student s){
        return this.getScore()-s.getScore();
    }
}
