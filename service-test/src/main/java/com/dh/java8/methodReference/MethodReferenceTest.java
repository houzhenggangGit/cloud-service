package com.dh.java8.methodReference;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 方法调用和方法引用没有实际关系
 *
 * 方法引用一共有四类
 *      1：类名::静态方法名
 *      2:引用名(对象名)::实例方法名
 *      3:类名::实例方法名
 *      4:类名::new （构造方法引用）
 */
public class MethodReferenceTest {


    //1类名::静态方法名
    @Test
    public void  test1(){
        Student s1=new Student("zs",10);
        Student s2=new Student("ls",20);
        Student s3=new Student("w5",30);
        Student s4=new Student("zl",20);
        List<Student> students= Arrays.asList(s1,s2,s3,s4);

        //jdk1.8提供了一个default sort方法
        students.sort((o1,o2) -> o1.getScore()-o2.getScore());
        //lamdba
        students.sort((student1,student2)->Student.compareStudentByScore(student1,student2));
        //方法引用 类名::静态方法名
        students.sort(Student::compareStudentByScore);

        students.forEach(System.out::println);
    }

    //引用名(对象名)::实例方法
    @Test
    public  void test2(){

        Student s1=new Student("zs",10);
        Student s2=new Student("ls",20);
        Student s3=new Student("w5",30);
        Student s4=new Student("zl",20);
        List<Student> students= Arrays.asList(s1,s2,s3,s4);

        CompareUtil util=new CompareUtil();
        //引用名(对象名)::实例方法
        students.sort(util::compareByScore);

        students.forEach(item-> System.out.println(item.getScore()));
    }

    //3:类名::实例方法名
    @Test
    public void test3(){

        Student s1=new Student("zs",10);
        Student s2=new Student("ls",20);
        Student s3=new Student("w5",30);
        Student s4=new Student("zl",20);
        List<Student> students= Arrays.asList(s1,s2,s3,s4);

        //方法的调用者实际上是lamdba表达式的第一个参数
        students.sort(Student::compare);

        students.forEach(item-> System.out.println(item.getScore()));

    }
    //类名::new （构造方法引用）
    @Test
    public void test_3(){
        List<String> list=Arrays.asList("beijing","qingdao","shagnhai");

        Collections.sort(list,(o1,o2)->o1.compareTo(o2));

        Collections.sort(list,String::compareToIgnoreCase);
    }

    @Test
    public void test4(){
        MethodReferenceTest test=new MethodReferenceTest();

        test.getString(String::new);

        System.out.println("=====================================");

        test.getString2("123",s1-> {return  new String(s1);});
        test.getString2("123",String::new);
    }

    public String getString(Supplier<String> supplier){
        return supplier.get();
    }

    public String getString2(String str, Function<String,String> function){
        return function.apply(str);
    }
}
