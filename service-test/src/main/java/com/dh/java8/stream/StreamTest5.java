package com.dh.java8.stream;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * stream 分组 group by ，分区partition by
 */
public class StreamTest5 {

    @Test
    public void test1(){
        List<String> list= Arrays.asList("hi","hello","你好");
        List<String> list1=Arrays.asList("张三","李四","王五");

        List<String> list3=list.stream().flatMap(
                item->list1.stream().map(item2->item+" " +item2)
        ).collect(Collectors.toList());
        list3.forEach(System.out::println);
    }

    /**
     * 按照姓名进行分组
     */
    @Test
    public void test2(){
        Student s1=new Student("zs",12);
        Student s2=new Student("zs",11);
        Student s3=new Student("ww",22);
        Student s4=new Student("zl",32);

        List<Student> li=Arrays.asList(s1,s2,s3,s4);

        Map<String,List<Student>> map=li.stream().collect(Collectors.groupingBy(Student::getName));

        System.out.println(map);
    }

    /**
     * select name ,count(name) from student group by student;
     */
    @Test
    public void test3(){
        //Collector
        Student s1=new Student("zs",12);
        Student s2=new Student("zs",11);
        Student s3=new Student("ww",22);
        Student s4=new Student("zl",32);

        List<Student> li=Arrays.asList(s1,s2,s3,s4);

        Map<String,Long> map=li.stream().collect(Collectors.groupingBy(Student::getName, Collectors.counting()));

        System.out.println(map);
    }

    /**
     * 对名字进行分组，求年龄的的平均值
     */
    @Test
    public void test4(){
        Student s1=new Student("zs",12);
        Student s2=new Student("zs",11);
        Student s3=new Student("ww",22);
        Student s4=new Student("zl",32);

        List<Student> li=Arrays.asList(s1,s2,s3,s4);

        Map<String,Double> map=li.stream().collect(Collectors.groupingBy(Student::getName, Collectors.averagingDouble(Student::getAge)));

        System.out.println(map);
    }

    @Test
    public void test5(){
        Student s1=new Student("zs",12);
        Student s2=new Student("zs",11);
        Student s3=new Student("ww",22);
        Student s4=new Student("zl",32);

        List<Student> li=Arrays.asList(s1,s2,s3,s4);

        Map<Boolean,List<Student>> map=li.stream().collect(
                Collectors.partitioningBy( ss-> ss.getAge()>20)
        );

        System.out.println(map);
    }
}
