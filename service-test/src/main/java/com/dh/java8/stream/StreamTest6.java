package com.dh.java8.stream;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import java.util.stream.Collectors.*;

public class StreamTest6 {

    /**
     * collect
     *         参数:Collector，是一个接口,有三个泛型（T,AR），可变的回去操作，
     *         将输入元素累计到一个可变的结果容器当中,他会在所有的元素都处理完毕后，
     *         将累积的结果转换为一个最终的表示（可选操作）；它支持串行和并行俩种操作。
     *         Collectors:提供了关于Collector的常见汇聚实现，Collectors本身是一个工厂。
     *         提供了四个接口：
     *           1:supplier --创建一个新的容器，没有输入元素
     *           2:accumator--将新的数据元素（流中的每个元素）合并到一个可变的结果容器
     *           3:combiner--将俩个结果容器合并成一个，与并行流紧密相关
     *           4:finisher--执行最终转换，从中间的累积类型转换为最终的结果类型（可选的）
     *          T
     *          A
     *          R
     *
     */
    @Test
    public void test5(){
        Student s1=new Student("zs",12);
        Student s2=new Student("zs",11);
        Student s3=new Student("ww",22);
        Student s4=new Student("zl",32);

        List<Student> li= Arrays.asList(s1,s2,s3,s4);

        List studentds=li.stream().collect(Collectors.toList());
        System.out.println("------------------------------------");
        //越具体的方式越好
        li.stream().collect(Collectors.counting());

        li.stream().count();
    }
}
