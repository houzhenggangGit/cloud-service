package com.dh.testsjsx;

/**
 * 输出结果：
 * Glypn() before draw()
 * ----:0
 * Glypn() after draw()
 * RoundGlyph() :5
 * --------------------------------------
 * new RoundGlyph(5);时不是默认初始值1，
 * 初始化的实际过程是
 * 1：在其他任何事物发生之前，将分配给对象的存储空间初始化成二进制的0
 * 2：调用基类构造器。（调用基类构造器之前先对基类构造器的成员初始化）此时，调用被覆盖后的draw()方法（要在调用子类的构造器之前调用），由于步骤1的缘故，radius为0
 * 3：导出类中按照声明的顺序调用成员的初始化方法。
 * 4：调用导出类的构造器。
 *
 */
class B{
    C c=new C();
    B(){
      System.out.println("B");
    }
}
class C{
    C(){
        System.out.println("C");
    }

}
class D extends B{
   D(){
       System.out.println("D");
   }
}

class Glypn{
    void draw(){
        System.out.println("Glypn.draw()");
    }
    Glypn(){
        System.out.println("Glypn() before draw()");
        draw();//0
        System.out.println("Glypn() after draw()");
    }
}
class RoundGlyph extends Glypn{
    private int radius=1;
    RoundGlyph(int r){
        radius=r;
        System.out.println("RoundGlyph() :"+radius);
    }
    void draw(){
        System.out.println("----:"+radius);
    }
}
public class PolyConstructors {
    public static void  main(String args[]){
        new RoundGlyph(5);
        new D();

    }
}
