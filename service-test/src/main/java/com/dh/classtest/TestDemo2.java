package com.dh.classtest;

interface Message{
    public void print();
}

abstract class Info{
    public abstract void get();
}

class MessageImpl extends Info implements Message{

    public void print() {

    }

    @Override
    public void get() {
    }
}

public class TestDemo2 {
}
