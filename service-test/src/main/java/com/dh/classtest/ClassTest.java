package com.dh.classtest;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/*class Message{

    private String name="123123";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}*/

public class ClassTest {

    public static void main(String args[]) throws  Exception{
        Class<?> cls=Class.forName("com.dh.classtest.Message");
        Object obj=cls.newInstance();
        //获得属性
        Field nameField=cls.getDeclaredField("name");
        //获得方法
        Method setmethod=cls.getDeclaredMethod("setName",nameField.getType());
        Method getmethod=cls.getDeclaredMethod("getName");
        setmethod.invoke(obj,"你好世界");
        System.out.println(getmethod.invoke(obj));
    }

    /**
     * 取得成员
     * getType()得到属性类型
     */
    @Test
    public void field() throws  Exception{
        Class<?> cls=Class.forName("com.dh.classtest.Message");
        Object obj=cls.newInstance();
        Field nameField=cls.getDeclaredField("name");
        nameField.setAccessible(true);//取消访问限制，可以访问私有属性
        nameField.set(obj,"nihao");
        System.out.println(nameField.get(obj));
    }
}
