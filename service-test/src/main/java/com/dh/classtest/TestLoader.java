package com.dh.classtest;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

class MyClassLoader extends ClassLoader{

    /**
     * loadClass
     * @param name
     * @return
     * @throws ClassNotFoundException
     */
    public Class<?> getClass(String name) throws ClassNotFoundException {
        byte[] data=null;
        try {
            data=loadClassFile(name);
        }catch (Exception e){
            e.printStackTrace();
        }
        return super.defineClass(name,data,0,data.length);
        //return super.loadClass(name);
    }

    private byte[] loadClassFile(String name) throws Exception{

        String fileName=name.substring(name.lastIndexOf(".")+1);
        String filepath="E:"+ File.separator+fileName;

        File file=new File(filepath);
        InputStream input=new FileInputStream(file);

        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        byte data[]=new byte[1024];
        int len=0;
        while((len=input.read(data))!=-1){
            baos.write(data,0,len);
        }
        input.close();
        baos.close();
        return baos.toByteArray();
    }
}

public class TestLoader {

    public static void  main(String args[]) throws Exception{
       /* System.out.println(new TestLoader().getClass().getClassLoader());
        System.out.println(new TestLoader().getClass().getClassLoader().getParent());*/
        MyClassLoader myClassLoader=new MyClassLoader();
        Class<?> cls=myClassLoader.getClass("java.util.Date");
        System.out.println(cls.newInstance());
    }
}
