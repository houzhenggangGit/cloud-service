package com.dh.util;

import java.io.Serializable;

import org.junit.Test;

import com.alibaba.fastjson.JSON;


public class InvokeResult implements Serializable, Cloneable {

	/** TODO */
	private static final long serialVersionUID = -5593722400156206443L;

	/**
	 * 业务调用是否成功。
	 */
	private boolean successful = true;

	/**
	 * 业务调用的返回提示信息。
	 */
	private String resultHint;

	/** 操作内容 */
	private String operContent;

	/**
	 * 业务调用的返回值。
	 */
	private Object resultValue;

	/**
	 * 业务层返回的错误代码
	 */
	private String errorCode;

	/**
	 * 默认构造函数。
	 */
	public InvokeResult() {
		clear();
	}

	/**
	 * 初始化返回信息。
	 */
	public void clear() {
		setSuccessful(false);
		setResultHint(null);
		setResultValue(null);
	}

	/**
	 * @param isSuccessful
	 *            the isSuccessful to set
	 */
	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}

	/**
	 * @return the isSuccessful
	 */
	public boolean getSuccessful() {
		return successful;
	}

	/**
	 * @param resultHint
	 *            the resultHint to set
	 */
	public void setResultHint(String resultHint) {
		this.resultHint = resultHint;
	}

	/**
	 * @return the resultHint
	 */
	public String getResultHint() {
		return resultHint;
	}

	/**
	 * @param resultValue
	 *            the resultValue to set
	 */
	public void setResultValue(Object resultValue) {
		this.resultValue = resultValue;
	}

	/**
	 * @return the resultValue
	 */
	public Object getResultValue() {
		return resultValue;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the operContent
	 */
	public String getOperContent() {
		return operContent;
	}

	/**
	 * @param operContent
	 *            the operContent to set
	 */
	public void setOperContent(String operContent) {
		this.operContent = operContent;
	}

	public String toJson() {
		return JSON.toJSONString(this);
	}

	/**
	 * 
	 * @Title: toBean
	 * @Description: 把Json串转化成InvokeResult，T为InvokeResult中ResultValue的类型信息
	 * @param
	 * @return InvokeResult
	 * @throws
	 */
	public static <T> InvokeResult toBean(String jsonString, Class<T> clazz) {
		InvokeResult result =JSON.parseObject(jsonString, InvokeResult.class);
		if (result.successful) {
			Object object = result.resultValue;
			jsonString=JSON.toJSONString(object);
			T obj = JSON.parseObject(jsonString,clazz);
			result.setResultValue(obj);
		}
		return result;
	}
	
	@Test
	public void test() {
		InvokeResult in=new InvokeResult();
		in.setSuccessful(true);
        in.setResultValue("123");
        in.setOperContent("123");
        System.out.println(in.toJson());
	}

}
