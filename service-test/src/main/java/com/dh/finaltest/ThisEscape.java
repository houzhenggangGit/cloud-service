package com.dh.finaltest;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;

public class ThisEscape {
	public final int id;
	public final String name;

	public ThisEscape(EventSource source) {
		id = 1;
		source.registerListener(new EventListener() {
			@Override
			public void handleEvent(Event evt) {
				// TODO Auto-generated method stub
				System.out.println("id: " + ThisEscape.this.id);
				System.out.println("name: " + ThisEscape.this.name);
				
			}
		});
		name = "flysqrlboy";

	}

	public static void main(String[] args) {
        EventSource<EventListener> source = new EventSource<EventListener>();
        ListenerRunnable listRun = new ListenerRunnable(source);
        Thread thread = new Thread(listRun);
        thread.start();
        ThisEscape escape1 = new ThisEscape(source);
  }
}

class EventSource<T> {
	private final List<T> eventListeners;

	public EventSource() {
		eventListeners = new ArrayList<T>();
	}

	public synchronized void registerListener(T eventListener) {
		this.eventListeners.add(eventListener);
		this.notifyAll();
	}

	public synchronized List<T> retrieveListeners() throws InterruptedException {
		List<T> dest = null;
		if (eventListeners.size() <= 0) {
			this.wait();
		}
		dest = new ArrayList<T>(eventListeners.size());
		dest.addAll(eventListeners);
		return dest;
	}
}

class ListenerRunnable implements Runnable {

	private EventSource<EventListener> source;

	public ListenerRunnable(EventSource<EventListener> source) {
		this.source = source;
	}

	public void run() {
		List<EventListener> listeners = null;

		try {
			listeners = this.source.retrieveListeners();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (EventListener listener : listeners) {
			//listener.onEvent(new Object());
			listener.toString();
		}
	}

}
