package com.dh.nio;

import java.nio.ByteBuffer;

import javax.swing.plaf.synth.SynthScrollBarUI;

import org.junit.Test;

/**
 * 1:缓冲区
 * ByteBuffer
 * charBuffer
 * shortBUffer
 * ...
 * 通过allocate()方法获取缓冲区
 * 
 * 2:核心方法
 * put()存入数据到缓冲区
 * get()获取数据
 * 
 * 3:核心属性
 * position：表示缓冲区正在操作的位置。
 * limit:界限，表示缓冲区可以操作数据的大小（limit后的数据不能进行读写）
 * capacity:最大容量
 * mark：记录position的位置，通过reset()mark的位置
 * 
 */
public class TestBuffer {
	
	public void test3(){
		String str="abcde";
		ByteBuffer buf=ByteBuffer.allocate(1024);
		buf.put(str.getBytes());
		
		buf.flip();
		byte[] dst=new byte[buf.limit()];
		buf.get(dst,0,2);
		
		buf.mark();
		
		buf.get(dst,2,2);
		
		buf.reset();//恢复到mark的位置
		System.out.println(buf.position());
		
	}
	
	@Test
	public void test2(){
		String str="abcde";
		ByteBuffer buf=ByteBuffer.allocate(1024);
		buf.put(str.getBytes());
		System.out.println("capacity-----"+buf.capacity());
		System.out.println("limit-----"+buf.limit());
		System.out.println("position-----"+buf.position());
		
		buf.flip();//转换读模式模式
		System.out.println("capacity-----"+buf.capacity());
		System.out.println("limit-----"+buf.limit());
		System.out.println("position-----"+buf.position());
		
		byte dst[]=new byte[buf.limit()];
		buf.get(dst);
		System.out.println(new String(dst,0,dst.length));
		System.out.println("capacity-----"+buf.capacity());
		System.out.println("limit-----"+buf.limit());
		System.out.println("position-----"+buf.position());
		
		//回到读模式，重复读数据
		buf.rewind();
		buf.get(dst);
		System.out.println("----"+new String(dst,0,dst.length));
		
		//清空缓冲区,数据依然存在，只是将position，limit重置为初始状态
		buf.clear();
		System.out.println((char)buf.get());
	}

}
