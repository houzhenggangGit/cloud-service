package com.dh.nio;

import org.junit.Test;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;

/**
 * 阻塞Io
 * 一：使用NIO完成网络通信的三个核心：
 * 1:通道Channel负责连接
 *   java.nio.channels.Channel接口
 *        |--SelectableChannel
 *           |--SocketChannel
 *           |--ServerSocketChannel
 *           |--DatagramChannel
 *
 *           |--Pipe.SinkChannel
 *           |--Pipe.SourceChannel、
 *
 *           FileChannel 不能切换为非阻塞模式，不能被选择器监控。
 * 2：缓冲区Buffer负责数据的存取
 * 3：选择器selector：是SelectableChannel的多路复用器，用于监控SelectableChannel的IO状况。
 */
public class TestBlockingNio {

    //客户端
    @Test
    public void client() throws  Exception{
        //获取通道
        SocketChannel sChannel = SocketChannel.open(new InetSocketAddress("127.0.0.1",9898));
        //用于读取本地文件的通道
        FileChannel inChannel=FileChannel.open(Paths.get("1.jpg"), StandardOpenOption.READ);

        //分配指定大小的缓冲区
        ByteBuffer buf=ByteBuffer.allocate(1024);

        //3读取本地文件，并发送到服务端、
        while (inChannel.read(buf)!=-1){
            buf.flip();
            sChannel.write(buf);
            buf.clear();
        }
        //关闭通道
        inChannel.close();
        sChannel.close();
    }

    //服务端
    @Test
    public void server()throws  Exception{
        //获取通道
        ServerSocketChannel ssChannel=ServerSocketChannel.open();
        //保存文件的通道
        FileChannel outChannel=FileChannel.open(Paths.get("3.jpg"), StandardOpenOption.WRITE,StandardOpenOption.CREATE_NEW);

        //绑定连接
        ssChannel.bind(new InetSocketAddress(9898));
        //获取客户端连接通道
        SocketChannel sChannel=ssChannel.accept();
        //接收数据，并保存。
        ByteBuffer buf=ByteBuffer.allocate(1024);
        while (sChannel.read(buf)!=-1){
            buf.flip();
            outChannel.write(buf);
            buf.clear();
        }
        //关闭通道
        ssChannel.close();
        outChannel.close();
        sChannel.close();
    }
}
