package com.dh.lock;

public class T1 {
	public static void main(String[] args) {
        PrivateNum p1=new PrivateNum();
    
        MyThread threadA=new MyThread('A',p1);
        MyThread2 threadB=new MyThread2('B',p1);
        threadA.start();
        threadB.start();
    }}
    class MyThread extends Thread
    {
        char i;
        PrivateNum p;
        public MyThread(char i,PrivateNum p)
        {
            this.i=i;
            this.p=p;
        }
        public void run()
        {
            p.test(i);
        }
    }
    class MyThread2 extends Thread
    {
        char i;
        PrivateNum p;
        public MyThread2(char i,PrivateNum p)
        {
            this.i=i;
            this.p=p;
        }
        public void run()
        {
            p.test2(i);
        }
    }
    class PrivateNum
    {
        int num=0;
        public void test2(char i)
        {
            System.out.println("线程"+i+"执行，线程A并没有同步执行");
        }
        public synchronized void  test( char i) 
        {
            
            try {
                
                if(i=='A')
                {
                    num=100;
                    
                    System.out.println("线程A已经设置完毕");
                    Thread.sleep(100);
                }
                else
                {
                    num=200;
                    System.out.println("线程B已经设置完毕");
                }
                System.out.println("线程"+i+"的值："+num);
                
            }
         catch (InterruptedException e) {
            // TODO 自动生成的 catch 块
            e.printStackTrace();
        }}
    }
