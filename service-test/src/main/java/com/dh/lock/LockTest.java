package com.dh.lock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockTest {

	static Lock lock = new ReentrantLock();
	static Condition c1 = lock.newCondition();
	static int status = 1;
	
	
	public static void main(String args[]) {
		System.out.println(status++==2);
	}

	static class Run1 implements Runnable {
		private LockTest lockTest;

		public Run1(LockTest lockTest) {
			this.lockTest = lockTest;
		}

		@Override
		public void run() {
			for (int i = 0; i < 5; i++) {
				try {
					lock.lock();
					while (status != 1) {
						try {
							c1.await();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					System.out.println("A");
					status = 2;
					c1.signal();
				} finally {
					lock.unlock();
				}
			}
		}
	}

	static class Run2 implements Runnable {

		private LockTest lockTest;

		public Run2(LockTest lockTest) {
			this.lockTest = lockTest;
		}

		@Override
		public void run() {
			for (int i = 0; i < 5; i++) {
				try {
					lock.lock();
					while (status != 2) {
						try {
							c1.await();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					System.out.println("B");
					status = 1;
					c1.signal();
				} finally {
					lock.unlock();
				}
			}
		}

	}

}
