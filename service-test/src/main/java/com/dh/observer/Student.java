package com.dh.observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * 观察者
 * 
 * @author ligb
 *
 */
public class Student implements java.util.Observer {

	private String name;

	public Student(String name) {
		this.name = name;
	}

	@Override
	public void update(Observable o, Object arg) {
		Teacher teacher = (Teacher) o;
		System.out.printf("学生%s观察到（实际是被通知）%s布置了作业《%s》 \n", this.name, teacher.getName(), arg);

	}

	public static void main(String[] args) {
		Student student1 = new Student("张三");
		Student student2 = new Student("李四");
		Teacher teacher1 = new Teacher("zuikc");
		teacher1.addObserver(student1);
		teacher1.addObserver(student2);
		teacher1.setHomework("事件机制第一天作业");
	}

}

/**
 * 被观察者
 * 
 * @author ligb
 *
 */
class Teacher extends java.util.Observable {

	private String name;
	private List<String> homeworks;

	public String getName() {
		return this.name;
	}

	public Teacher(String name) {
		this.name = name;
		homeworks = new ArrayList<String>();
	}

	public void setHomework(String homework) {
		System.out.printf("%s布置了作业%s \n", this.name, homework);
		homeworks.add(homework);
		setChanged();
		notifyObservers(homework);

	}
}