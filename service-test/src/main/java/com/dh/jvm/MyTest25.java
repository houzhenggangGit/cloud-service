package com.dh.jvm;

public class MyTest25 implements Runnable{
    private Thread thread;

    public  MyTest25(){
        thread=new Thread(this);
        thread.run();
    }
    @Override
    public void run() {
        ClassLoader classLoader=this.thread.getContextClassLoader();
        //System.out.println(classLoader);
        this.thread.setContextClassLoader(classLoader);

        System.out.println("Class:---"+classLoader.getClass());
        System.out.println("Parent:---"+classLoader.getParent().getClass());

    }

    public static void main(String[] args) {
        new MyTest25();
    }
}
