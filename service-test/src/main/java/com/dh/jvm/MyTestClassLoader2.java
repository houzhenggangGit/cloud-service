package com.dh.jvm;

import org.junit.Test;

import java.net.URL;
import java.util.Enumeration;

public class MyTestClassLoader2 {
    public static void main(String[] args) {
        ClassLoader classLoader=ClassLoader.getSystemClassLoader();

        System.out.println(classLoader);

        while(null!=classLoader){
            classLoader=classLoader.getParent();
            System.out.println(classLoader);
        }
    }

    /**
     * 获得一个类的calss文件 的全路劲
     * @throws Exception
     */
    @Test
    public void test1() throws  Exception{
        //appclassLoader
        ClassLoader classLoader=Thread.currentThread().getContextClassLoader();
        String name="com/dh/jvm/MyTestClassLoader2.class";

        Enumeration<URL> urls= classLoader.getResources(name);

        while(urls.hasMoreElements()){
            System.out.println(urls.nextElement());
        }
    }
}
