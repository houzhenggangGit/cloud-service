package com.dh.jvm;

public class MyTestClassLoader1 {
    public static void main(String[] args) throws Exception{
        Class<?> clazz=Class.forName("java.lang.String");
        //如果是启动类加载器会返回一个null
        System.out.println(clazz.getClassLoader());

        Class<?> c=Class.forName("com.dh.jvm.C");
        System.out.println( c.getClassLoader());

    }

}

class C{

}

