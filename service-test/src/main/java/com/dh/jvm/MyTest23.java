package com.dh.jvm;

import sun.misc.Launcher;

/**
 * 内建于jvm中的启动类加载器会加载java.lang,ClassLoader以及其他的java平台类
 * 当jvm启动时，一块特殊的机器码会运行，他会加载扩展类加载器与系统类加载器，
 * 这块特殊的机器码叫做启动类加载器
 *
 * 启动类加载器并不是java类，而其他的加载器则都是java类
 * 启动类加载器是特定于平台的机器指令，他负责开启整个加载过程
 *
 * 启动类加载器还会负责加载供jre正常运行所需的基本组件，包括java.util java.lang等
 */
public class MyTest23 {
    public static void main(String[] args) throws Exception{

        System.out.println(System.getProperty("sun.boot.class.path"));
        System.out.println(System.getProperty("java.ext.dirs"));
        System.out.println(System.getProperty("java.class.path"));

        //扩展类加载器与系统类加载器也是由启动类加载器加载的
        System.out.println(Launcher.class.getClassLoader());
        //改变系统类加载，使用我们自定义的类加载器
        //java -Djava.system.class.loader=com.dh.jvm.MyClassLoader  com.dh.jvm.MyTest23
        System.out.println(System.getProperty("java.system.class.loader"));
        //x系统类加载器为MyClassLoader
        System.out.println(ClassLoader.getSystemClassLoader());
        System.out.println("------------------------------");
        /**
         *
         * initialize:是否需要初始化
         */
        MyClassLoader loader1=new MyClassLoader("loader1");
        Class<?> c=Class.forName("com.dh.jvm.MyTest26",true,loader1);
        c.newInstance();
        System.out.println(c.getClassLoader());
        System.out.println(Class.class.getClassLoader());
    }
}
