package com.dh.jvm;
class Parent{
    public  static  int a;
    static{
        System.out.println("Parent static block");
    }
}
class Child extends Parent{
    public  static  int b;
    static {
        System.out.println("Child static block");
    }
}

public class MyTest8 {

    static {
        System.out.println("MyTest8 static block");
    }

    public static void main(String[] args) {
        /**
         * 输出如下
         * MyTest8 static block
           Parent static block
           0
         */
        //System.out.println(Child.a);

        /**
         MyTest8 static block
         Parent static block
         Child static block
         0
         */
        System.out.println(Child.b);

    }
}
