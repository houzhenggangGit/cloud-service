package com.dh.jvm;

import java.util.UUID;

/**
 *
 * 编译时不能确定的常量的值，不会被放入调用类的常量池中
 * 这时在程序运行时，会导致主动使用这个常量所在的类，会导致这个类被初始化
 */
public class MyTest3 {
    public static void main(String[] args) {
        //MyParent3 static block
        //uuid
        System.out.println(MyParent3.str);

    }
}

class MyParent3{
    public static final String str= UUID.randomUUID().toString();
    static {
        System.out.println("MyParent3 static block");
    }
}