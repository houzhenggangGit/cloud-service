package com.dh.jvm;

/**
 * Singleton.getSingleton()对类的主动使用，会触发初始化，初始化之前准备阶段会赋默认值
 *
 * 准备阶段为成员变量赋默认值，
 * Singleton.getSingleton()初始化，真正赋值。
 *
 */
public class MyTest6 {
    public static void main(String[] args) {
        Singleton singleton=Singleton.getSingleton();
        System.out.println(Singleton.counter1);//1
        System.out.println(Singleton.counter2);//0
    }
}

class Singleton{
    public static int counter1;

    private static Singleton singleton=new Singleton();

    private Singleton(){
        counter1++;
        counter2++;
    }

    public static int counter2=0;

    public static Singleton getSingleton(){
        return singleton;
    }
}