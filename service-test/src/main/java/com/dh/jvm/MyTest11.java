package com.dh.jvm;

import java.lang.reflect.Method;

public class MyTest11 {
    public static void main(String[] args) throws Exception{
        MyClassLoader loader1=new MyClassLoader("loader1");
        loader1.setPath("E:\\gitlabcloud\\classes\\");
        MyClassLoader loader2=new MyClassLoader("loader2");

        Class<?> clazz=loader1.loadClass("com.dh.jvm.MyPerson");
        Object o=clazz.newInstance();
        Method method=clazz.getMethod("getPdrso",Object.class);
        method.invoke(o,o);
    }
}
