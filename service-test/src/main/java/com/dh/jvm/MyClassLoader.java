package com.dh.jvm;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class MyClassLoader extends ClassLoader{

    private String classLoaderName;

    private final  String fileExtension=".class";

    private String path;

    public void setPath(String path) {
        this.path = path;
    }

    public MyClassLoader(String classLoaderName){
        super();//将系统类加载当做父类加载器
        this.classLoaderName=classLoaderName;
    }
    public  MyClassLoader(ClassLoader parent,String classLoaderName){
        super(parent);
        this.classLoaderName=classLoaderName;
    }

    public MyClassLoader(ClassLoader parent){
        super(parent);
    }
    @Override
    protected Class<?> findClass(String className){
        byte[] bytes=this.loadClassDate(className);
        //defineClass把我二进制解析成Class的实例
        return this.defineClass(className,bytes,0,bytes.length);
    }

    private  byte[] loadClassDate(String classname){
        InputStream is=null;
        byte[] date=null;
        ByteArrayOutputStream baos=null;

        classname=classname.replace(".","\\");

        try {
            //this.classLoaderName=this.classLoaderName.replace(".","/");

            is=new FileInputStream(new File(this.path+classname+this.fileExtension));
            baos=new ByteArrayOutputStream();

            int ch=0;

            while(-1!=(ch=is.read())){
                baos.write(ch);
            }
            date=baos.toByteArray();
        }catch (Exception e){
            e.printStackTrace();
        }
        return date;
    }



    public static void test(ClassLoader classLoader) throws  Exception{
        Class<?> clazz=classLoader.loadClass("com.dh.jvm.MyParent1");
        Object object=clazz.newInstance();
        System.out.println(object);
    }

    /**
     * 如果class文件在classpath下，自己定义的类加载不会执行，
     * 加载扎个类的加载器是当前自定义加载的父类加载（应用类加载器），
     * 但是，当加载的class文件不在classpath下，由于父类加载器找不到class文件，所以
     * 自定义的加载将会执行。
     *
     * 如果class文件位于classpath下，连续加载多次（appClassLoader加载，只加载一次），也只会存在一个Class,
     * 但是当Class文件位于其他地方连续加载多次，会存在多个Class实列（因为他们的命名空间不同）
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception{
        MyClassLoader myClassLoader=new MyClassLoader("111");
        myClassLoader.setPath("D:\\gitnew\\cloud-service\\service-test\\target\\classes\\");
        Class<?> clazz=myClassLoader.loadClass("com.dh.jvm.MyParent1");
        System.out.println(clazz.newInstance());
        System.out.println(clazz.hashCode());

        System.out.println("-------------------");

        MyClassLoader myClassLoader1=new MyClassLoader("111");
        myClassLoader1.setPath("D:\\gitnew\\cloud-service\\service-test\\target\\classes\\");
        Class<?> clazz1=myClassLoader1.loadClass("com.dh.jvm.MyParent1");
        System.out.println(clazz1.newInstance());
        System.out.println(clazz1.hashCode());

    }

}
