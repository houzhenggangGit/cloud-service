package com.dh.jvm;

import com.sun.crypto.provider.AESKeyGenerator;

/**
 * 扩展类加载器：加载类时需要达成jar包的形式
 * java -Djava.ext.dirs=./ com.xx.xx.xx
 */
public class MyTest10 {
    public static void main(String[] args) {
        AESKeyGenerator aesKeyGenerator=new AESKeyGenerator();
        System.out.println(aesKeyGenerator.getClass().getClassLoader());
        System.out.println(MyTest10.class.getClassLoader());

    }


}
