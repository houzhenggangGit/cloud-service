package com.dh.jvm;

public class MyTest22 {

    static {
        System.out.println("MyTest22 static");
    }

    /**
     * 指定扩展类加载器的路劲，扩展类加载器默认加载jar包下的文件。
     * 所以要想让某个类让扩展类加载器加载需要将类打成jar包
     * 打包：java -Djava.ext.dirs=./ com.dh.jvm.MyTest22
     * 执行：java -Djava.ext.dirs=./ com.dh.jvm.MyTest22
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(MyTest22.class.getClassLoader());
        System.out.println(MyTest1.class.getClassLoader());

    }
}
