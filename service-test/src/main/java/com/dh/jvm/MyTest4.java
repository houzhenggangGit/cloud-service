package com.dh.jvm;

/**
 * 对于数组来说，其类型是由jvm在运行期动态生成的。不会触发MyParent4的
 */
public class MyTest4 {
    public static void main(String[] args) {
        MyParent4[] myParent4=new MyParent4[1];
    }
}

class MyParent4{
    static {
        System.out.println("MyParent3 static block");
    }
}
