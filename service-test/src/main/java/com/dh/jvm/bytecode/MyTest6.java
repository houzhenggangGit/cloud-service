package com.dh.jvm.bytecode;

/**
 * 方法的动态分派
 * 方法的动态分派涉及到一个重要概念：方法接受者（谁调用）
 * 方法重载是静态的，编译期行为
 * 方法重写是动态的，运行期行为
 */
public class MyTest6 {
    public static void main(String[] args) {
        Fruit apple=new Appale();
        Fruit orange=new Orange();

        apple.test();
        orange.test();
    }
}
class Fruit{
    public void test(){
        System.out.println("Fruit");
    }
}
class Appale extends Fruit{
    @Override
    public void test() {
        System.out.println("Apple");
    }
}
class Orange extends Fruit{
    @Override
    public void test() {
        System.out.println("Orange");
    }
}