1:使用 javap -verbose命令分析一个字节码文件时，将会分析该字节码文件的魔数，版本号，常量池，类信息，类的构造方法
类中的方法信息，类变量与成员变量等信息

2.魔数：所有的.class字节码文件的前四个字节都是魔数，魔数值为定值：0xCA FE BA BE(咖啡宝贝)

3.魔数之后的四个字节为版本信息（ 00 00 00 34 ），前俩个字节为次版本号，后俩个字节为主版本号

4.常量池：紧接着主版本号之后的就是常量池入口，一个java类中定义的很多信息都是由常量池来维护和描述的，
  我们可以将常量池看做class文件的资源仓库。比如 方法与变量信息 都是存储在常量池。常量池中主要存储俩类
  常量：字面量，符号引用
    字面量：文本字符串，java中声明为final的常量值。
    符号引用：类和接口的全局限定名（包名.类名），字段的名称和描述符，方法的名称和描述符等。
  
5.常量池的总体结构：java类所对应的常量池主要由产量池数量与常量池常量池数组这俩部分共同构成。
    (1)常量池数量紧跟在主版本号后边，占据俩个字节。
    (2)常量池数组则紧跟在常量池数量之后。常量池数组与一般的数组不同的是，常量池数组中不同的元素的类型，结构都是不同的
  但是每一种元素的第一个数据都是u1类型，该字节是个标志位，占据一个字节。jvm在解析常量池时，会根据这个u1类型来获取元素
  的具体类型。
  
  注意： 常量池数组中的元素的个数=常量池数量-1(其中0暂时不使用)，目的是满足某些常量池索引值的数据在特定情况下需要表达【不引用任何一个常量池】的含义
  根本原因：索引0也是一个常量（保留常量），它不位于常量表中，这个常量就对应null值；所以，常量池的索引从1而非0开始。

6.在jvm规范中，每个变量/字段都有描述信息，描述信息主要作用是描述字段的数据类型，方法的参数列表（包括数量，类型，顺序）与返回值。根据描述符规则，基本数据类型
  和代表返回值的void 类型都用一个大写字符来表示，对象类型则使用字符L加对象的全限定名老表示。为了压缩字节码问文件的体积
  对于基本数据类型，jvm都只使用一个大写字母表示。
  B-byte,
  C-char,
  D-double,
  I-int,
  Z-booelan,
  J-long,
  V-void,
  L-对象类型  如Ljava/lang/String; 

7.对于数组类型来说，每一个维度使用一个 [ 来表示 int[] 被记录为[I  String[]被记录为[Ljava/lang/String

8.用描述符描述方法时，按照先参数列表，后返回值的顺序来描述。参数列表按照顺序放在（）内
 如String getByName(int id,String name)表述为：（I,Ljava/lang/String） Ljava/lang/String

9.java中的实例方法一定会有一个局部变量this（方法参数也是局部变量）。成员变量的赋值操作默认的是在无参构造方法中。






使用javap -c 反编译的文件
"C:\Program Files\Java\jdk1.8.0_171\bin\javap.exe" -verbose com.dh.jvm.bytecode.MyTest1
Classfile /D:/gitnew/cloud-service/service-test/target/classes/com/dh/jvm/bytecode/MyTest1.class
  Last modified 2019-4-28; size 485 bytes
  MD5 checksum 07a0e589fda91ea21d43cc3c1e26558f
  Compiled from "MyTest1.java"
public class com.dh.jvm.bytecode.MyTest1
  minor version: 0
  major version: 52
  flags: ACC_PUBLIC, ACC_SUPER
Constant pool:
   #1 = Methodref          #4.#20         // java/lang/Object."<init>":()V
   #2 = Fieldref           #3.#21         // com/dh/jvm/bytecode/MyTest1.a:I
   #3 = Class              #22            // com/dh/jvm/bytecode/MyTest1
   #4 = Class              #23            // java/lang/Object
   #5 = Utf8               a
   #6 = Utf8               I
   #7 = Utf8               <init>
   #8 = Utf8               ()V
   #9 = Utf8               Code
  #10 = Utf8               LineNumberTable
  #11 = Utf8               LocalVariableTable
  #12 = Utf8               this
  #13 = Utf8               Lcom/dh/jvm/bytecode/MyTest1;
  #14 = Utf8               getA
  #15 = Utf8               ()I
  #16 = Utf8               setA
  #17 = Utf8               (I)V
  #18 = Utf8               SourceFile
  #19 = Utf8               MyTest1.java
  #20 = NameAndType        #7:#8          // "<init>":()V
  #21 = NameAndType        #5:#6          // a:I
  #22 = Utf8               com/dh/jvm/bytecode/MyTest1
  #23 = Utf8               java/lang/Object
{
  public com.dh.jvm.bytecode.MyTest1();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=2, locals=1, args_size=1
         0: aload_0
         1: invokespecial #1                  // Method java/lang/Object."<init>":()V
         4: aload_0
         5: iconst_1
         6: putfield      #2                  // Field a:I
         9: return
      LineNumberTable:
        line 3: 0
        line 5: 4
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0      10     0  this   Lcom/dh/jvm/bytecode/MyTest1;

  public int getA();
    descriptor: ()I
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=1, args_size=1
         0: aload_0
         1: getfield      #2                  // Field a:I
         4: ireturn
      LineNumberTable:
        line 8: 0
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       5     0  this   Lcom/dh/jvm/bytecode/MyTest1;

  public void setA(int);
    descriptor: (I)V
    flags: ACC_PUBLIC
    Code:
      stack=2, locals=2, args_size=2
         0: aload_0
         1: iload_1
         2: putfield      #2                  // Field a:I
         5: return
      LineNumberTable:
        line 12: 0
        line 13: 5
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       6     0  this   Lcom/dh/jvm/bytecode/MyTest1;
            0       6     1     a   I
}
SourceFile: "MyTest1.java"