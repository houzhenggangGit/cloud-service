package com.dh.jvm.bytecode;

/**
 * javap -verbose -p com.dh.jvm.bytecode.MyTest2
 -p参数可以反编译出private的方法。
 对于synchronized修饰的实例方法：给当前对象枷锁
 synchronized修饰的静态方法：给当前类的class对象加锁

 synchronized 代码块：monitorenter 代码块进入的入口
                     monitorexit 方法退出时的出口，保证异常抛出时能对锁释放

 如果我们没有显示的写构造方法java 编译器会默认生成一个不带参数的构造方法，
 构造方法的主要作用是给成员变量赋值（值是从常量池中寻找的）
 如果有多个构造方法，编译器会把成员变量拷贝多次，每个构造方法中都会有一份成员变零的赋值操作

 对于静态变量编译器会生成一个clinit，静态变量会在clinit中完成赋值操作
多个静态代码块会合并成一个clinit


 */
public class MyTest2 {

    String str="Welcome";

    private int x=5;

    public static Integer in=10;

    public static void main(String[] args) {
        MyTest2 myTest2=new MyTest2();

        myTest2.setX(8);

        in = 20;

    }
    private synchronized void setY(int x){
        this.x=x;
    }
    public void setX(int x){
        this.x=x;
    }
    private void test1(String str){
        synchronized (str){
            System.out.println("hello world");
        }
    }
    private synchronized  static void test2(){

    }
}
