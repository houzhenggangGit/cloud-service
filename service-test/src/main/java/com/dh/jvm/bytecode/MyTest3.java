package com.dh.jvm.bytecode;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;

/**
 * 对于java类中的每一个实例方法（非static方法），再其编译后所生成的字节码当中，方法参数的数量总是
 * 比源代码中参数个数多一个（this），他位于方法的第一个参数位置处；这样我们就可以在java实例方法中
 * 用this来访问当前对象的属性以及方法
 *
 * 这个操作是在编译期间完成的，即由javac编译器在编译的时候对this的访问转化为一个普通方法参数的访问。
 * 接下来在运行期间，由JVM在调用实例方法时，自动向该实例方法传入this。所以在实例方法的局部变量表中，至少会有一个this
 */

/**
 * java字节码对于异常的处理方式
 *      1：统一采用异常表的方式来对异常进行处理‘
 *      2：当异常处理存在finally语句时，现在花的jvm处理方式是将finally语句块的字节码拼接到每个cache块后面
 *
 */
public class MyTest3 {

    public void test(){
        try{
            InputStream is=new FileInputStream("123.txt");

            ServerSocket serverSocket=new ServerSocket(999);
            serverSocket.accept();

        }catch (FileNotFoundException ex){

        }catch (IOException ex){

        }catch (Exception ex){

        }finally {
            System.out.println("dd");
        }
    }
}
