package com.dh.jvm;

public class MySample {

    public MySample() {
        System.out.println("MySample classLoader:"+this.getClass().getClassLoader());
        new MyCat();
    }

    /**
     * 类加载并不需要等到某个类被首次使用时才加载，jvm会根据上下文预先加载这个类
     * -XX:+TraceClassLoading,用于追踪类的加载信息并被打印出来
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception{
        MyClassLoader loader1=new MyClassLoader("loader1");
        Class<?> clazz=loader1.loadClass("com.dh.jvm.MySample");
        System.out.println(clazz.getClassLoader());
        //clazz.newInstance();
    }
}
