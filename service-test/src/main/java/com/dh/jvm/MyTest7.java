package com.dh.jvm;

/**
 * 当java虚拟机初始化一个类时，要求他的所有父类都已经被初始化，这条规则不适用于接口
 *           -在初始化一个类时，并不会优先初始化他所实现的接口
 *           -初始化一个接口时，并不会先初始化它的父接口
 *       因此一个父接口，并不会因为它的子接口或者实现类的初始化而初始化，只用当程序首次特定接口的静态变量，才会
 *       导致该接口的初始化
 */
public class MyTest7 {
    public static void main(String[] args) {
        System.out.println(MyChild7.b);//在初始化一个类时，并不会优先初始化他所实现的接口
        System.out.println("--------------------------------------------------");
        System.out.println(Son.thhread);//初始化一个接口时，并不会先初始化它的父接口
    }
}

interface MyParent7{
    public static  Thread thhread=new Thread(){
        {
            System.out.println("MyParent7 invoke");
        }
    };
}

class MyChild7 implements MyParent7{
    public static   int b=6;
}

interface Father{
    public static  Thread thhread=new Thread(){
        {
            System.out.println("BaBa invoke");
        }
    };
}
//初始化一个接口时，并不会先初始化它的父接口
interface Son{
    public static  Thread thhread=new Thread(){
        {
            System.out.println("Son invoke");
        }
    };
}


