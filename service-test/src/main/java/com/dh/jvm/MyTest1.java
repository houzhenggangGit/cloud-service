package com.dh.jvm;

public class MyTest1 {
    public static void main(String[] args) {
        /**
         * 输出：MyParent1 static block
         *      hello
         * 这种情况输出 MyChild1没有被初始化
         * 对于静态字段来说，只有直接定义了该字段的类才会被初始化。
         * -XX:+TraceClassLoading,用于追踪类的加载信息并被打印出来
         * -XX:++TraceClassUnloading,用于跟踪类的卸载信息并被打印出来
         */
        System.out.println(MyChild1.str);

        /**
         * MyParent1 static block
         * MyChild1 static block
         * world
         * 当一个子类初始化时，要求其父类都已经初始化完毕了
         *
         */
        System.out.println(MyChild1.str2);
    }
}
class MyParent1{
    public static String str="hello";

    static {
        System.out.println("MyParent1 static block");
    }
}

class MyChild1 extends MyParent1{
    public static String str2="world";

    static {
        System.out.println("MyChild1 static block");
    }
}
