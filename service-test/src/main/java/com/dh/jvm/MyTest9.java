package com.dh.jvm;

class CL{
    static {
        System.out.println("Class CL");
    }
}

public class MyTest9 {
    /**
     * 调用ClassLoader的loadClass方法加载一个类，并不是对类的主动使用，不会导致类的初始化
     * Class.forName("com.dh.jvm.CL") 主动使用
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception{
        ClassLoader loader=ClassLoader.getSystemClassLoader();
        Class<?> calzz=loader.loadClass("com.dh.jvm.CL");//这行代码不会导致类被初始化
        System.out.println(calzz);
        System.out.println("---------------");
        calzz=Class.forName("com.dh.jvm.CL");//会对类进行初始化
        System.out.println(calzz);
    }
}
