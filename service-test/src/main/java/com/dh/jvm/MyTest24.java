package com.dh.jvm;

import javax.xml.bind.SchemaOutputResolver;

/**
 * 当前类加载器（current classloader）
 *      每个类都会使用自己的类加载器来加载其所依赖的类
 *      如果A类引用了B类，那么A类的加载器将会去加载B(前提是B还未被加载)
 *  线程上下文类加载器（Context ClassLoader）
 *      线程上下文类加载器从jdk1.2引入，类中的getContextClassLoader()和setContextClassLoader()
 *      分别用来设置上下文类加载器。
 *
 *      如果没有设置线程上下文加载器，线程将继承其父线程的上下文类加载器。
 *      java应用运行时的初始线程的上下文加载器是系统加载器（appClassLoader）。在线程中运行的代码可以通过该类加载器加载
 *
   线程上下文类加载器的重要性:
 *      父ClassLoader可以使用当前线程Thread.currentThread().getContextClassLoader()所指定的classLoader加载的类。
 *      这就改变了父ClassLoader不能使用子ClassLoader或是没有父子关系的ClassLoader加载的类的情况,即改变了双亲
 *      委托模型
 *     Example: JDBC连接
 *             Class.forName("xxx.xxx.xxx");
 *             Connection conn=Driver.getConnection()
 *                      Connection 位于核心类库，是由根类加载加载
 *                      Driver位于ClassPath是由系统类加载器加载。通过给当前线程设置上下文类加载器，就可以
 *                      由设置的上下文类加载器来实现对于接口实现类的加载。
 *
 *
 */
public class MyTest24 {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getContextClassLoader());//appClassLoader
        System.out.println(Thread.class.getClassLoader());//null 根类加载器
    }
}
