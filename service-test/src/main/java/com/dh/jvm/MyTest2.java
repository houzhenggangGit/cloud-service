package com.dh.jvm;

/**
 *常量在编译阶段会存入到调用这个常量的方法所在的类的常量池中。
 * 本质上调用类并没有（MyTest2）直接引用到定义常量的类，因此不会触发MyParent2的初始化
 * 注意：这里指的是将常量放到MyTest2 的常量池中，之后MyTest2与MyParent2就没有任何关系了
 * 甚至我们可以将MyParent2的class文件删除。
 *
 * 反编译命令 javap -c src
 */
public class MyTest2 {
    public static void main(String[] args) {
        //只输出hello world
        System.out.println(MyParent2.str);
    }
}

class MyParent2{
    public static final String str="hello world";

    static {
        System.out.println("MyParent2 static block");
    }

}
