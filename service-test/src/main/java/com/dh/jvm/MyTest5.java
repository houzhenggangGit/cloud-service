package com.dh.jvm;

/**
 * 接口加载阶段验证，MyChild5.b会放入MyTest5的常量池中。
 * 删除MyParent5.class MyChild5.class程序依然可以正常运行。
 */
public class MyTest5 {
    public static void main(String[] args) {
        System.out.println(MyChild5.b);
    }
}

interface MyParent5{
    public static  final  int a=5;
}

interface MyChild5 extends MyParent5{
    public static  final  int b=6;
}
