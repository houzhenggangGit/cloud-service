package com.dh.stack;


/**
 * 基于数组实现的栈的实现
 * @author liguobao
 * 
 * 栈主要包含两个操作，入栈和出栈，也就是在栈顶插入一个数据和从栈顶删除一个数据。
 * 理解了栈的定义之后，我们来看一看如何用代码实现一个栈。
 * <p>实际上，栈既可以用数组来实现，也可以用链表来实现。用数组实现的栈，我们叫作<strong>顺序栈</strong>，
 * 用链表实现的栈，我们叫作<strong>链式栈</strong>。</p>
 *
 */
public class ArrayStack {
	private String[] items;
	private int count;//栈中的元素个数
	private int n;//栈的大小
	
	/**
	 * 初始化数组，申请一个大小为N的数组空间
	 * @param n
	 */
	public ArrayStack(int n) {
		this.items=new String[n];
		this.n=n;
		this.count=0;
	}
	
	/**
	 * 入栈操作
	 * @param item
	 * @return
	 */
	public boolean push(String item) {
		//判断栈是否有空闲
		if(count==n) {
			return false;
		}
		items[count]=item;
		count++;
		return true;
	}
	
	/**
	 * 出栈操作
	 * @return
	 */
	public String get() {
		if(count==0)
			return null;
		String str=items[count-1];
		items[count-1]=null;
		count--;
		return str;
	}

	public static  void main(String args[]) {
		ArrayStack array=new ArrayStack(10);
		array.push("1");
		array.push("2");
		array.push("3");
		String str=array.get();
		System.out.println(str);
	}

}
