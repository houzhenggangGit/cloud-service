package com.dh.heap;


public class Test12 {
	 static final Object object =new Object(); //共享对象，用来实现对象锁
	 public static void main(String args[]) {
		 new Thread(new Runnable() {
			@Override
			public void run() {
				for(int i=0;i<100;i++) {
					synchronized (object) {
						try {
							System.out.println("a");
							object.notify();//唤醒线程2
							object.wait();//当前线程释放对象锁
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}).start();
		 
		new Thread(new Runnable() {
			@Override
			public void run() {
				for(int i=0;i<100;i++) {
					synchronized(object) {
						try {
							System.out.println("b");
							object.notify();
							object.wait();					
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
			}
		}).start();; 
	 }
}
