package org.influxdb.use;

import org.influxdb.BatchOptions;
import org.influxdb.InfluxDB;
import org.influxdb.TestUtils;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;
import org.influxdb.dto.QueryResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RunWith(JUnitPlatform.class)
public class BasicUsageTest {

    InfluxDB influxDB;

    /**
     * 获得influxDb连接
     * @throws InterruptedException
     * @throws IOException
     */
   /* @BeforeEach
    public void setUp() throws InterruptedException, IOException {
         this.influxDB = InfluxDBFactory.connect("http://192.168.91.132:8086", "admin", "DahaoNetwork2018");
    }*/


    /**
     * 这是一种推荐的将数据点写入数据库的方法。
     * 客户端将写操作存储到内部缓冲区中，并以固定的刷新间隔异步刷新它们，
     * 以在客户端和服务器端获得良好的性能。
     */
    @Test
    public void test1(){
        InfluxDB influxDB =  InfluxDBFactory.connect("http://192.168.91.132:8086", "admin", "DahaoNetwork2018");
        String dbName = "aTimeSeries";
        influxDB.createDatabase(dbName);
        influxDB.setDatabase(dbName);
        String rpName = "aRetentionPolicy";
        influxDB.createRetentionPolicy(rpName, dbName, "30d", "30m", 2, true);
        influxDB.setRetentionPolicy(rpName);

        influxDB.enableBatch(BatchOptions.DEFAULTS);

        influxDB.write(Point.measurement("cpu")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("idle", 90L)
                .addField("user", 9L)
                .addField("system", 1L)
                .build());

        influxDB.write(Point.measurement("disk")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("used", 80L)
                .addField("free", 1L)
                .build());

        Query query = new Query("SELECT idle FROM cpu", dbName);
        QueryResult result= influxDB.query(query);
        System.out.println(result.toString());
        influxDB.dropRetentionPolicy(rpName, dbName);
        influxDB.deleteDatabase(dbName);
        influxDB.close();
    }

    /**
     * 批处理刷新期间发生的任何错误都不会泄漏到write方法的调用者中。
     * 默认情况下，任何类型的错误都将被记录为“严重”级别。
     * 如果在发生此类异步错误时需要通知您并执行一些自定义逻辑，
     * 您可以使用重载enableBatch方法添加一个带有BiConsumer<Iterable<Point>，
     * Throwable>的错误处理程序:
     */
    @Test
    public void test2(){
        influxDB.enableBatch(BatchOptions.DEFAULTS.exceptionHandler(
                (failedPoints, throwable) -> {

                })
        );
    }

    /**
     * 如果您的点被写入不同的数据库
     */
    public  void test3(){
        InfluxDB influxDB =  InfluxDBFactory.connect("http://192.168.91.132:8086", "admin", "DahaoNetwork2018");
        String dbName = "aTimeSeries";
        influxDB.createDatabase(dbName);//创建数据库
        String rpName = "aRetentionPolicy";
        influxDB.createRetentionPolicy(rpName, dbName, "30d", "30m", 2, true);

        // Flush every 2000 Points, at least every 100ms
        influxDB.enableBatch(BatchOptions.DEFAULTS.actions(2000).flushDuration(100));

        Point point1 = Point.measurement("cpu")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("idle", 90L)
                .addField("user", 9L)
                .addField("system", 1L)
                .build();
        Point point2 = Point.measurement("disk")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("used", 80L)
                .addField("free", 1L)
                .build();

        influxDB.write(dbName, rpName, point1);
        influxDB.write(dbName, rpName, point2);
        Query query = new Query("SELECT idle FROM cpu", dbName);
        influxDB.query(query);
        influxDB.dropRetentionPolicy(rpName, dbName);
        influxDB.deleteDatabase(dbName);
        influxDB.close();
    }

    /**
     * 同步写
     * 如果您希望立即将数据点写入到流感数据库(并处理错误)，而不需要任何延迟，请参见下面的示例:
     */
    @Test
    public void test4(){
        InfluxDB influxDB =  InfluxDBFactory.connect("http://192.168.91.132:8086", "admin", "DahaoNetwork2018");
        String dbName = "aTimeSeries";
        influxDB.createDatabase(dbName);
        String rpName = "aRetentionPolicy";
        influxDB.createRetentionPolicy(rpName, dbName, "30d", "30m", 2, true);

        BatchPoints batchPoints = BatchPoints
                .database(dbName)
                .tag("async", "true")
                .retentionPolicy(rpName)
                .consistency(InfluxDB.ConsistencyLevel.ALL)
                .build();
        Point point1 = Point.measurement("cpu")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("idle", 90L)
                .addField("user", 9L)
                .addField("system", 1L)
                .build();
        Point point2 = Point.measurement("disk")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("used", 80L)
                .addField("free", 1L)
                .build();
        batchPoints.point(point1);
        batchPoints.point(point2);
        influxDB.enableGzip();//压缩数据
        influxDB.write(batchPoints);
        Query query = new Query("SELECT idle FROM cpu", dbName);
        QueryResult result=influxDB.query(query);
        System.out.println(result.toString());
        influxDB.dropRetentionPolicy(rpName, dbName);
        influxDB.deleteDatabase(dbName);
    }

    /**
     *
     */
    public void test5(){
        try (InfluxDB influxDBs = InfluxDBFactory.connect("http://172.17.0.2:8086", "root", "root")) {
            // Read or Write, do any thing you want
        }
    }

    @Test
    public void test6(){
        InfluxDB influxDB =  InfluxDBFactory.connect("http://192.168.91.132:8086", "admin", "DahaoNetwork2018");
        String dbName = "aTimeSeries";
        influxDB.createDatabase(dbName);//创建数据库
        String rpName = "aRetentionPolicy";
        influxDB.createRetentionPolicy(rpName, dbName, "30d", "30m", 2, true);
        //prepare data
        List<String> lineProtocols = new ArrayList<String>();
        int i = 0;
        int length = 0;

        Point point = Point.measurement("udp_single_poit")
                .addField("v", i)
                .build();
        String lineProtocol = point.lineProtocol();
        lineProtocols.add(lineProtocol);
        influxDB.write(8096, point);
    }
}
