package org.influxdb.dto;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * 查询语句封装类
 */
public class Query {

  /**查询命令*/
  private final String command;
  /**数据库*/
  private final String database;
  /**是否为post*/
  private final boolean requiresPost;

  /**
   * 构造函数默认requiresPost为false
   * @param command 查询命令
   * @param database 查询数据库
   */
  public Query(final String command, final String database) {
    this(command, database, false);
  }

   /**
    * 构造函数
   * @param command 查询命令
   * @param database 数据库
   * @param requiresPost true为 post请求反之为get
   */
   public Query(final String command, final String database, final boolean requiresPost) {
    super();
    this.command = command;
    this.database = database;
    this.requiresPost = requiresPost;
  }

  /**
   * @return the command
   */
  public String getCommand() {
    return this.command;
  }

  /**
   * @return url encoded command
   */
  public String getCommandWithUrlEncoded() {
    return encode(this.command);
  }

  /**
   * @return the database
   */
  public String getDatabase() {
    return this.database;
  }

  public boolean requiresPost() {
    return requiresPost;
  }

  @SuppressWarnings("checkstyle:avoidinlineconditionals")
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((command == null) ? 0 : command.hashCode());
    result = prime * result
        + ((database == null) ? 0 : database.hashCode());
    return result;
  }

  @SuppressWarnings("checkstyle:needbraces")
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Query other = (Query) obj;
    if (command == null) {
      if (other.command != null)
        return false;
    } else if (!command.equals(other.command))
      return false;
    if (database == null) {
      if (other.database != null)
        return false;
    } else if (!database.equals(other.database))
      return false;
    return true;
  }

  /**
   * 格式化命令
   * @param command
   *            命令
   * @return 格式化后的命令
   */
  public static String encode(final String command) {
    try {
      return URLEncoder.encode(command, StandardCharsets.UTF_8.name());
    } catch (UnsupportedEncodingException e) {
      throw new IllegalStateException("Every JRE must support UTF-8", e);
    }
  }
}
