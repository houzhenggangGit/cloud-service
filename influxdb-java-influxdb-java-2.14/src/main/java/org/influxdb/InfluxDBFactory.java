package org.influxdb;

import org.influxdb.InfluxDB.ResponseFormat;
import org.influxdb.impl.InfluxDBImpl;

import okhttp3.OkHttpClient;
import org.influxdb.impl.Preconditions;

import java.util.Objects;


/**
 *InfluxDB Database 工厂
 */
public enum InfluxDBFactory {
  INSTANCE;

  /**
   * 创建InfluxDB 连接，无验证模式
   *
   * @param url
   *          InfluxDB url
   * @return
   *         InfluxDB
   */
  public static InfluxDB connect(final String url) {
    Preconditions.checkNonEmptyString(url, "url");
    return new InfluxDBImpl(url, null, null, new OkHttpClient.Builder());
  }

  /**
   * 密码模式连接 InfluxDB
   *
   * @param url
   *            连接路劲
   * @param username
   *            账号
   * @param password
   *            密码
   * @return a InfluxDB adapter suitable to access a InfluxDB.
   */
  public static InfluxDB connect(final String url, final String username, final String password) {
    Preconditions.checkNonEmptyString(url, "url");
    Preconditions.checkNonEmptyString(username, "username");
    return new InfluxDBImpl(url, username, password, new OkHttpClient.Builder());
  }

  /**
   * 创建连接
   *
   * @param url
   *            连接路劲
   * @param client
   *           OkHttpClient.Builder
   * @return a InfluxDB adapter suitable to access a InfluxDB.
   */
  public static InfluxDB connect(final String url, final OkHttpClient.Builder client) {
    Preconditions.checkNonEmptyString(url, "url");
    Objects.requireNonNull(client, "client");
    return new InfluxDBImpl(url, null, null, client);
  }

  /**
   * 创建连接密码模式，返回 Response body 为Json格式
   *
   * @param url
   *            连接路劲
   * @param username
   *            账号
   * @param password
   *           密码
   * @param client
   *            OkHttpClient.Builder
   * @return a InfluxDB adapter suitable to access a InfluxDB.
   */
  public static InfluxDB connect(final String url, final String username, final String password,
      final OkHttpClient.Builder client) {
    return connect(url, username, password, client, ResponseFormat.JSON);
  }

  /**
   * 创建连接密码模式，自定义Response body返回类型
   *
   * @param url
   *            the url to connect to.
   * @param username
   *            the username which is used to authorize against the influxDB instance.
   * @param password
   *            the password for the username which is used to authorize against the influxDB
   *            instance.
   * @param client
   *            the HTTP client to use
   * @param responseFormat
   *            The {@code ResponseFormat} to use for response from InfluxDB server
   * @return a InfluxDB adapter suitable to access a InfluxDB.
   */
  public static InfluxDB connect(final String url, final String username, final String password,
      final OkHttpClient.Builder client, final ResponseFormat responseFormat) {
    Preconditions.checkNonEmptyString(url, "url");
    Preconditions.checkNonEmptyString(username, "username");
    Objects.requireNonNull(client, "client");
    return new InfluxDBImpl(url, username, password, client, responseFormat);
  }
}
