package com.dahaonetwork.smartfactory.constant;

import org.springframework.boot.bind.RelaxedPropertyResolver;


public enum  Platform {
    INSTANCE;
    private RelaxedPropertyResolver propertyResolver;
    private String baseDir;
    
    private final static String DEFAULT_PREFIX="platform.config.";
    
    /** config.xml 是否集群 配置项关键字 */
    public final static String CLUSTER = "cluster";
    /** config.xml 是否调试模式 配置项关键字 */
    public final static String DEBUGMODE = "debugMode";
    /** config.xml 是否应用portal */
    public final static String ISPORTAL = "isPortal";
    /** 是否进行登陆密码加密 */
    public final static  String ENCRYPTPW = "encryptPW";
    /** 是否单页面方式打开菜单 */
    public final static String SINGLEPAGE = "singlePage";
    /** 集群 */
    private int clusterFlag = -1;
    /** 调试模式 */
    private int debuggerFlag = -1;
    /** 登陆密码加密 */
    private int encryptPWFlag = -1;
    /** 单页面方式打开模块 */
    private int singlePageFlag = -1;
    /** 全局jqueryAjax异常切片 */
    private int globalAjaxAdviceFlag = -1;

    void setResolver(RelaxedPropertyResolver resolver) {
        this.propertyResolver = resolver;
    }

    /**
     * @Title getString
     * @Description 获取具有平台默认前缀:platform.config 的配置项
     * @param key 配置项名
     * @return String 配置值
     */
    public String getString(String key) {
        return this.getString(DEFAULT_PREFIX, key);
    }
    
    /**
     * @Title: getNoPrefixString
     * @Description: 获取自定义前缀的配置项，若无，传""
     * @param key 配置项名
     * @return String 配置值
     */
    public String getString(String prefix, String key){
    	return propertyResolver.getProperty(prefix + key);
    }
    

    public String getDbTpye() {
        return getString("dbtype");
    }

    public static  Platform getPlatform(){
        return INSTANCE;
    }

}
