package com.dahaonetwork.smartfactory.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.client.RestTemplate;



public class BaseAgent {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());  


	/** rest请求模板 */
	@Autowired
	protected RestTemplate restTemplate;
	/** 微服务id */
	protected String serviceId;
	
	/** 监听实现类bean名称 */
	protected String beanName;
	/**
	 * 获取头信息
	 * 
	 * @Title: getHeader
	 * @Description: TODO ribbon方式重构请求头
	 * @return
	 */
	protected HttpHeaders setHeader() {
		HttpHeaders headers = new HttpHeaders();
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
			headers.add("Authorization", "bearer " + details.getTokenValue());
		} catch (Exception e) {
			//e.printStackTrace();
			log.error("获取tocken异常", e);
		}
		return headers;
	}
	

	public String getServiceId() {
		return serviceId;
	}


	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}


	public String getBeanName() {
		return beanName;
	}


	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

}
