package com.dahaonetwork.smartfactory.websocket.model;

public enum MessageType {
	
	whole("发送消息体Json字符串"),content("仅发送消息内容");
	
	
	/** 描述 */
	private String desc;
	
	private MessageType(){}
	
	private MessageType(String desc){
		this.desc = desc;
	}
	
	public String getDesc(){
		return desc;
	}
	
	public boolean isMe(String type){
		return type.equals(this.name()) ? true : false;
	}
}