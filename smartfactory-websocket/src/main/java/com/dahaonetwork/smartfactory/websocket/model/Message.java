package com.dahaonetwork.smartfactory.websocket.model;

public class Message {
	/** 主题Id */
	private String topicId;
	/** 标识Id */
	private String identityId;
	/** 消息内容 */
	private String content;
	/** 自定义标明发送者 */
	private String sender;
	
	/** 消息发送类型： */
	private String type;
	
	public Message(){}
	
	public Message(String topicId,String identityId,String content,String sender,String type){
		this.topicId = topicId;
		this.identityId = identityId;
		this.content = content;
		this.sender = sender;
		this.type = type;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	public String getTopicId() {
		return topicId;
	}
	
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	
	public String getIdentityId() {
		return identityId;
	}
	
	public void setIdentityId(String identityId) {
		this.identityId = identityId;
	}
	
	/**
	 * @Title: getType
	 * @Description: 默认是使用传递消息信息
	 * @return MessageType
	 */
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}

}
