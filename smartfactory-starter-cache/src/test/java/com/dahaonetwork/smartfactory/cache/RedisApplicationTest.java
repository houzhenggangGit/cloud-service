package com.dahaonetwork.smartfactory.cache;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dahaonetwork.smartfactory.cache.dao.RedisDao;


@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=CacheApplication.class)
public class RedisApplicationTest {
	
	@SuppressWarnings("rawtypes")
	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private CacheTestService cacheTestService;
	
	@Autowired
	private ReadlockTest readlockTest;
	
	@Autowired
	private RedisDao redisDao;
	
	/** 
	 * @Title: test 
	 * @Description: 测试redisTemplate操作 
	 * @throws Exception
	 */
	//@Test
	public void test() throws Exception {
		// 保存对象
		User user = new User("超人", 20);
		redisTemplate.opsForValue().set(user.getUsername(), user);
		user = new User("蝙蝠侠", 30);
		redisTemplate.opsForValue().set(user.getUsername(), user);
		user = new User("蜘蛛侠", 40);
		redisTemplate.opsForValue().set(user.getUsername(), user);
		Assert.assertEquals(20, ((User) redisTemplate.opsForValue().get("超人")).getAge().longValue());
		Assert.assertEquals(30, ((User) redisTemplate.opsForValue().get("蝙蝠侠")).getAge().longValue());
		Assert.assertEquals(40, ((User) redisTemplate.opsForValue().get("蜘蛛侠")).getAge().longValue());
	}

	/** 
	 * @Title: cacheTest 
	 * @Description: 测试spring缓存操作 
	 * @throws Exception
	 */
	//@Test
	public void cacheTest() throws Exception {
		cacheTestService.findUser("123", "张三", 2);
		cacheTestService.findUser("123", "张三", 2);
	}
	/*@Test
	public void delete() throws Exception {
		cacheTestService.delete("123");
	}*/
	
	@Test
	public void delete() throws Exception {
		redisDao.remove("batchDetailCache:"+130);
	}
	
	
	//@Test
	public void cacheKey() throws Exception {
		User user=new User();
		user.setAge(12);
		user.setId("123");
		user.setIds("123123");
		cacheTestService.find3(user);
		cacheTestService.find3(user);
	}
	
	//@Test
	public void lockTest() throws Exception {
		readlockTest.testRedlock();
	}

}
