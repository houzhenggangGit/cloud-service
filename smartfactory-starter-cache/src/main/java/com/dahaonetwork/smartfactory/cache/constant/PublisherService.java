package com.dahaonetwork.smartfactory.cache.constant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


/**
 * 消息发布
 * @author liguobao
 *
 */
public class PublisherService {

	private RedisTemplate redisTemplate;
	
	@Autowired
	public PublisherService(RedisTemplate redisTemplate) {
		this.redisTemplate=redisTemplate;
	}

	public String sendMessage(String name) {
		try {
			redisTemplate.convertAndSend("TOPIC_USERNAME", name);
			redisTemplate.convertAndSend("CHANEL", name);
			return "消息发送成功了";

		} catch (Exception e) {
			e.printStackTrace();
			return "消息发送失败了";
		}
	}

}
