package com.dahaonetwork.smartfactory.cache.configure;

import java.lang.reflect.Method;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.dahaonetwork.smartfactory.cache.constant.PublisherService;
import com.dahaonetwork.smartfactory.cache.constant.Receiver;
import com.dahaonetwork.smartfactory.cache.redis.RedisObjectSerializer;

@Configuration
@EnableCaching
public class CacheConfigure /* extends CachingConfigurerSupport */ {

	/** 日志 */
	private Log log = LogFactory.getLog(CacheConfigure.class);

	/**
	 * @Title: wiselyKeyGenerator @Description: 缓存key生成 @param @return
	 * KeyGenerator @throws
	 */
	@Bean
	public KeyGenerator wiselyKeyGenerator() {
		return new KeyGenerator() {
			public Object generate(Object target, Method method, Object... params) {
				StringBuilder sb = new StringBuilder();
				sb.append(target.getClass().getName());
				sb.append(method.getName());
				for (Object obj : params) {
					sb.append(obj.toString());
				}
				return sb.toString();
			}
		};
	}

	/**
	 * @Title: cacheManager @Description: 缓存管理器 @param redisTemplate
	 * 使用redis做为缓存的基础 @return CacheManager @throws
	 */
	@Bean("cacheManager")
	public CacheManager cacheManager(@SuppressWarnings("rawtypes") RedisTemplate redisTemplate) {
		log.info("缓存管理器已创建");
		RedisCacheManager redisCacheManager = new RedisCacheManager(redisTemplate);
		// Number of seconds before expiration. Defaults to unlimited (0)
		// redisCacheManager.setDefaultExpiration(); //设置key-value超时时间
		redisCacheManager.setUsePrefix(true);
		return redisCacheManager;
	}

	/**
	 * @Title: redisTemplate @Description: redis操作模板 @param @return
	 * RedisTemplate<String,Object> @throws
	 */
	@Bean("redisTemplate")
	public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
		RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
		template.setConnectionFactory(factory);
		template.setKeySerializer(new StringRedisSerializer());
		//template.setValueSerializer(new RedisObjectSerializer());
		 //设置序列化Value的实例化对象
		template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
		return template;
	}
    
	

	// ---------------------------------订阅发布客户端注册--------------------------------------------
	/*@Bean
	CountDownLatch latch() {
		return new CountDownLatch(1);
	}*/

	/**
	 * 消息发送者
	 * @param redisTemplate
	 * @return
	 */
	@Bean("publisherService")
	PublisherService publisherService(RedisTemplate redisTemplate) {
		return new PublisherService(redisTemplate);
	}
	
	/**
	 * 默认消息接收者，监听器需要监听消息接收者,客户端注入
	 * @return
	 */
	@Bean("receiver")
	Receiver receiver() {
		return new Receiver();
	}
    
	 /**
     * 创建消息监听容器
     *
     * @param redisConnectionFactory
     * @param messageListenerAdapter
     * @return
     */
	@Bean
	RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
			MessageListenerAdapter listenerAdapter,MessageListenerAdapter listenerAdapter1) {

		RedisMessageListenerContainer container = new RedisMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.addMessageListener(listenerAdapter, new PatternTopic("TOPIC_USERNAME"));
        
		container.addMessageListener(listenerAdapter1, new PatternTopic("CHANEL"));
		return container;
	}
    
	/**
     * 消息监听适配器，注入接受消息方法，输入方法名字 反射方法
     *
     * @param receiver
     * @return
     */
	@Bean
	MessageListenerAdapter listenerAdapter(Receiver receiver) {
		return new MessageListenerAdapter(receiver, "receiveMessage");
	}
	
	@Bean
	MessageListenerAdapter listenerAdapter1(Receiver receiver) {
		return new MessageListenerAdapter(receiver, "receiveMessage1");
	}

}
