package com.dahaonetwork.smartfactory.cache.constant;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 消息订阅
 * @author liguobao
 *
 */
public class Receiver {
	private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);

	//private CountDownLatch latch;

	@Autowired
	public Receiver() {
		
	}

	public void receiveMessage(String message) {
		LOGGER.info("管道1");
		LOGGER.info("Received <" + message + ">");
		//latch.countDown();
	}
	
	public void receiveMessage1(String message) {
		LOGGER.info("管道2");
		LOGGER.info("Received <" + message + ">");
		//latch.countDown();
	}

}
