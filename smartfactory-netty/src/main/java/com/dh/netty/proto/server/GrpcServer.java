package com.dh.netty.proto.server;

import com.dh.netty.proto.serviceImpl.PersonServiceGrpcImpl;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class GrpcServer {
    private Server server;

    private void start() throws IOException {
        this.server= ServerBuilder.forPort(8899).addService(new PersonServiceGrpcImpl()).build().start();

        Runtime.getRuntime().addShutdownHook(new Thread(()->{
            GrpcServer.this.stop();
        }));
    }

    private void stop(){
        if(null!=server){
            this.server.shutdown();
        }
    }

    private void awaitTermination() throws InterruptedException {
        if(null!=this.server){
            this.server.awaitTermination();
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        GrpcServer grpcServer=new GrpcServer();
        grpcServer.start();
        grpcServer.awaitTermination();
    }

}
