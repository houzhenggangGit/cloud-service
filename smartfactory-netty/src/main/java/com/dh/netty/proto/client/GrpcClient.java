package com.dh.netty.proto.client;

import com.dh.netty.proto.model.MyResponse;
import com.dh.netty.proto.model.MyRequest;
import com.dh.netty.proto.model.StudentRequest;
import com.dh.netty.proto.model.StudentsResponse;
import com.dh.netty.proto.service.PersonServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.Iterator;

public class GrpcClient {
    public static void main(String[] args) throws  Exception{
        ManagedChannel managedChannel= ManagedChannelBuilder.forAddress("localhost",8899).usePlaintext().build();

        PersonServiceGrpc.PersonServiceBlockingStub blockingStub=PersonServiceGrpc.newBlockingStub(managedChannel);

        MyRequest myRequest=MyRequest.newBuilder().setUsername("123").build();
        MyResponse myResponse=blockingStub.getRealNameByUsername(myRequest);

        System.out.println(myResponse.getRealname());
        //
        System.out.println("---------------------------------------");
        Iterator<StudentsResponse> iterator= blockingStub.getEntityByAge(StudentRequest.newBuilder().setAge(20).build());

        while (iterator.hasNext()){
            StudentsResponse studentsResponse=iterator.next();
            System.out.println(studentsResponse.toString());
        }
    }
}
