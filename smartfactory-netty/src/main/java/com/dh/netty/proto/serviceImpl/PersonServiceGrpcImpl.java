package com.dh.netty.proto.serviceImpl;


import com.dh.netty.proto.model.MyRequest;
import com.dh.netty.proto.model.MyResponse;
import com.dh.netty.proto.model.StudentRequest;
import com.dh.netty.proto.model.StudentsResponse;
import com.dh.netty.proto.service.PersonServiceGrpc;
import io.grpc.stub.StreamObserver;

public class PersonServiceGrpcImpl extends PersonServiceGrpc.PersonServiceImplBase{

    @Override
    public void getRealNameByUsername(MyRequest request, StreamObserver<MyResponse> responseObserver) {
        System.out.println("接收到客户端 的消息:"+request.getUsername());

        //将消息返回给客户端
        responseObserver.onNext(MyResponse.newBuilder().setRealname("lisi").build());
        responseObserver.onCompleted();
    }

    @Override
    public void getEntityByAge(StudentRequest request, StreamObserver<StudentsResponse> responseObserver) {
        System.out.println("接收到客户端的消息"+request.getAge());

        responseObserver.onNext(StudentsResponse.newBuilder().setAge("20").setName("张三").build());
        responseObserver.onNext(StudentsResponse.newBuilder().setAge("10").setName("李四").build());
        responseObserver.onNext(StudentsResponse.newBuilder().setAge("30").setName("王五").build());
        responseObserver.onNext(StudentsResponse.newBuilder().setAge("40").setName("赵六").build());

        responseObserver.onCompleted();
    }


}
