package com.dh.netty.discard.server.discard;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
/**
 * 处理服务端server
 * ChannelInboundHandlerAdapter，这个类实现了
 * ChannelInboundHandler接口，ChannelInboundHandler 提供了许多事件处理的接口方法，
 * 然后你可以覆盖这些方法。
 * @author liguobao
 *
 */
public class DiscardServerHandler extends ChannelInboundHandlerAdapter{
	/**
	 * 这里我们覆盖了 chanelRead() 事件处理方法。每当从客户端收到新的数据时，这个方法会
	 * 在收到消息时被调用，这个例子中，收到的消息的类型是 ByteBuf
	 * @param ctx
	 * @param msg
	 */
	@Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
 		/**
 		 * 为了实现DISCARD协议，处理程序必须忽略收到的消息。
		 * ByteBuf是一个引用计数对象，必须通过release（）方法显式释放。
 		 * 请记住，处理程序有责任释放传递给处理程序的引用计数对象。
		 * 通常，channelRead（）处理方法的实现方式如下：
 		 */
		ByteBuf in = (ByteBuf) msg;
		try {
			//ctx.writeAndFlush(msg);
			while (in.isReadable()) {
				System.out.print((char) in.readByte());
				System.out.flush();
			}
		} finally {
			/*//丢弃ByteBuf对象*/
			ReferenceCountUtil.release(msg);
		}
    }
    
 	/**
 	 * 异常捕获()事件处理程序方法在Netty由于I/O错误或处理程序实现由于在处理事件时抛出异常而引发异常时被一次性调用。
 	 * 在大多数情况下，应该对捕获的异常进行日志记录，并关闭其关联的通道，
 	 * 尽管此方法的实现可能因您希望如何处理异常情况而有所不同。
 	 * 例如，您可能希望在关闭连接之前发送带有错误代码的响应消息。
 	 */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }

	@Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
    	ctx.flush();
	}


}
