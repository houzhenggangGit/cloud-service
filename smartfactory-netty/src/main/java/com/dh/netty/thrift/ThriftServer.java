package com.dh.netty.thrift;

import com.dh.netty.thrift.generated.PersonService;
import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.THsHaServer;
import org.apache.thrift.server.TServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;

public class ThriftServer {
    public static void main(String[] args) throws  Exception{
        TNonblockingServerSocket socket=new TNonblockingServerSocket(8899);
        THsHaServer.Args arg=new THsHaServer.Args(socket).minWorkerThreads(2).maxWorkerThreads(4);

        PersonService.Processor<PersonServiceImpl> processor=new PersonService.Processor<>(new PersonServiceImpl());

        arg.protocolFactory(new TCompactProtocol.Factory());//协议层
        arg.transportFactory(new TFramedTransport.Factory());//传输层
        arg.processorFactory(new TProcessorFactory(processor));

        //THsHaServer 半同步半异步，服务模型
        TServer server=new THsHaServer(arg);

        System.out.println();
        //死循环 异步
        server.serve();

    }
}
