package com.dh.netty.thrift;

import com.dh.netty.thrift.generated.Person;
import com.dh.netty.thrift.generated.PersonService;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

public class ThriftClient {
    public static void main(String[] args) {
        TTransport transport=new TFramedTransport(new TSocket("localhost",8899),600);
        TProtocol protocol=new TCompactProtocol(transport);

        PersonService.Client client=new PersonService.Client(protocol);

        try{
            transport.open();

            Person person=client.getPersonByUsername("zhangsan");
            System.out.println(person.toString());
            System.out.println("----------------------");

            Person p=new Person();
            p.setUsername("123").setMarried(false).setAge(12);
            client.savePerson(person);

        }catch (Exception e){
            throw  new RuntimeException(e.getMessage(),e);
        }finally {
            transport.close();
        }
    }
}
