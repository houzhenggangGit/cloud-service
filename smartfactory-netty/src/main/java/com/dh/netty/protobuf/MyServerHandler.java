package com.dh.netty.protobuf;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class MyServerHandler extends SimpleChannelInboundHandler<DateInfo.Mymessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, DateInfo.Mymessage s) {
        if(s.getDateType()==DateInfo.Mymessage.DateType.StudentType){
            DateInfo.Student  student   =s.getStudent();
            System.out.println(student.toString());
        }else if(s.getDateType()==DateInfo.Mymessage.DateType.DogType){
            DateInfo.Dog  dog   =s.getDog();
        }else{
            DateInfo.Cat  cat   =s.getCat();
        }
    }
}
