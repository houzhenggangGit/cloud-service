package com.dh.netty.protobuf;

public class ProtoBufTest {
    public static void main(String[] args) throws Exception{
        DateInfo.Student student=DateInfo.Student.newBuilder().setAddress("北京").setAge(20).setName("zhangsan")
                .build();

        byte[] bytestudent=student.toByteArray();

        DateInfo.Student s= DateInfo.Student.parseFrom(bytestudent);
        System.out.println(s.toString());
    }
}
