package com.dh.netty.protobuf;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class MyClientHandler extends SimpleChannelInboundHandler<DateInfo.Mymessage>{


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DateInfo.Mymessage s) throws Exception {
        System.out.println("client output:"+s);
        ctx.writeAndFlush("from clent:"+3333);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        DateInfo.Mymessage mymessage=DateInfo.Mymessage.newBuilder().
                setDateType(DateInfo.Mymessage.DateType.StudentType).
                setStudent(
                        DateInfo.Student.newBuilder().setName("zhagnsan")
                                .setAge(20).setAddress("beijing")
                        .build()
                ).build();
        ctx.writeAndFlush(mymessage);
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

}
