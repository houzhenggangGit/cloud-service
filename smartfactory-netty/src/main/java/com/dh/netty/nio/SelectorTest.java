package com.dh.netty.nio;

import io.netty.buffer.ByteBuf;
import org.junit.jupiter.api.Test;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.Iterator;
import java.util.Set;

/**
 * 双向通信
 * 创建时通过系统底层选择器创建
 * selector注册到channnel，选择器维护三种selectorKey集合。
 *   （1）keys() 所有集合的全集(不能直接被修改)
 *    (2)selectedkeys() 感兴趣的key
 *    (3)取消的key,
 *  对于一个新创建的selector，三个key都为空。
 *  channnel.register(),
 *
 *
 */
public class SelectorTest {

    @Test
    public void server() throws Exception{

        int[] port=new int[]{5001,5002,5003,5004};

        Selector selector=Selector.open();

         for (int i=0;i<port.length;i++){
             ServerSocketChannel serverSocketChannel=ServerSocketChannel.open();
             serverSocketChannel.configureBlocking(false);
             ServerSocket serverSocket=serverSocketChannel.socket();
             InetSocketAddress address=new InetSocketAddress(port[i]);
             serverSocket.bind(address);//绑定
             // channnel 注册到selector.并在注册过程中指出该信道可以进行Accept操作
             serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

             System.out.println("监听端口："+port[i]);
         }

         while (true){
             int numbers=selector.select();//阻塞的
             System.out.println("numbers:"+numbers);

             Set<SelectionKey> selectionKeys = selector.selectedKeys();

             System.out.println("selectionKeys"+selectionKeys);

             Iterator<SelectionKey> iter=selectionKeys.iterator();
             while (iter.hasNext()){
                 SelectionKey selectionKey=iter.next();
                 if(selectionKey.isAcceptable()){
                     ServerSocketChannel serverSocketChannel=(ServerSocketChannel) selectionKey.channel();

                     SocketChannel socketChannel=serverSocketChannel.accept();
                     socketChannel.configureBlocking(false);

                     socketChannel.register(selector,SelectionKey.OP_READ);
                     iter.remove();//必须删除
                     System.out.println("获得客户端连接："+socketChannel);
                 }else if (selectionKey.isReadable()){
                     SocketChannel socketChannel=(SocketChannel)selectionKey.channel();
                     int bytesRead=0;
                     while (true){
                         ByteBuffer byteBuffer=ByteBuffer.allocate(512);
                         byteBuffer.clear();
                         int read=socketChannel.read(byteBuffer);
                         if(read<=0){
                             break;
                         }
                         byteBuffer.flip();
                         socketChannel.write(byteBuffer);
                         bytesRead+=read;
                     }
                     System.out.println("读取数据："+bytesRead);
                     iter.remove();
                 }
             }

         }













    }

}
